import feedparser
from discord.ext import tasks
import discord.utils
from discord.ext import commands
import logging
import sys


class YouTubeFeeds(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
        self.get_youtube_feeds.change_interval(seconds=self.config["interval"])
        self.get_youtube_feeds.start()
    
    def cog_unload(self):
        self.get_youtube_feeds.cancel()

    def _load_config(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "youtube-feeds"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["channel"] = tmp[0]
        self.config["role"] = tmp[1]
        self.config["interval"] = tmp[2]
    
    def _get_feeds(self):
        self.bot.database.cursor.execute('SELECT idchannel FROM "youtube-feeds-list"')
        tmp = self.bot.database.cursor.fetchall()
        tmp = [item for t in tmp for item in t] 
        return tmp

    def _downloaded(self, video):
        self.bot.database.cursor.execute('SELECT id FROM "youtube-feeds-downloaded" WHERE id = ? AND feed = ?', video)
        tmp = self.bot.database.cursor.fetchall()
        return len(tmp)
    
    def _save_feed(self, feed):
        self.bot.database.cursor.execute('INSERT INTO "youtube-feeds-downloaded" (id, title, feed, date) VALUES (?,?,?,?)', feed)
        self.bot.database.c.commit()

    def _get_conf(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "youtube-feeds"')
        return self.bot.database.cursor.fetchone()
    
    def _get_all_feeds(self):
        self.bot.database.cursor.execute('SELECT idchannel,description FROM "youtube-feeds-list"')
        return self.bot.database.cursor.fetchall()
    
    def _get_downloaded_feeds(self):
        self.bot.database.cursor.execute('SELECT id,title FROM "youtube-feeds-downloaded" order by date')
        return self.bot.database.cursor.fetchall()
    
    def _remove_download(self, id):
        self.bot.database.cursor.execute('DELETE FROM "youtube-feeds-downloaded" WHERE id = ?',(id,))
        self.bot.database.c.commit()
    
    def _add_feed(self,name,id):
        self.bot.database.cursor.execute('INSERT INTO "youtube-feeds-list" (idchannel, description) VALUES (?,?)',(id,name))
        self.bot.database.c.commit()
    
    def _remove_feed(self,name):
        self.bot.database.cursor.execute('DELETE FROM "youtube-feeds-list" WHERE description = ?',(name,))
        self.bot.database.c.commit()
    
    def _clear_download(self):
        self.bot.database.cursor.execute('DELETE FROM "youtube-feeds-downloaded"')
        self.bot.database.c.commit()
    
    def _set_conf(self, channel, role, interval):
        self.bot.database.cursor.execute('DELETE FROM  "youtube-feeds"')
        self.bot.database.cursor.execute('INSERT INTO  "youtube-feeds" (channel, role, interval) VALUES (?,?,?)',(channel,role,interval))
        self.bot.database.c.commit()

    async def __youtube(self, ctx):
        conf = self._get_conf() 
        msg = "__**Youtube feeds configuration**__\n"
        await ctx.send(msg + "\n**Channel**: " + conf[0] + "\n**Role**: " + conf[1] + "\n**Interval**: " + str(conf[2]) + " seconds")
    
    async def __youtube_feeds(self, ctx):
        feeds = self._get_all_feeds()
        msg = "__**Youtube feeds list**__\n"
        for feed in feeds:
            msg += "\n**" + feed[1] + "** [" + feed[0] + "]"
        await ctx.send(msg)
    
    async def __youtube_downloads(self, ctx):
        feeds = self._get_downloaded_feeds()
        msg = "__**Youtube downloaded videos**__\n"
        await ctx.send(msg) 
        for feed in feeds:
            await ctx.send("\n**" + feed[1] + "** [" + feed[0] + "]")
        
    async def __youtube_conf(self, ctx, channel, role, interval):
        self._set_conf(channel, role, interval)
        await self.__youtube(ctx)

    async def __youtube_clear(self, ctx):
        self._clear_download()
        await ctx.send("✅ All videos removed")
   
    async def __youtube_remove_feed(self, ctx, name):
        self._remove_feed(name)
        await self.__youtube_feeds(ctx)

    async def __yourube_remove_download(self, ctx, id):
        self._remove_download(id)
        await ctx.send("✅ Video [**" + id + "**] removed")

    async def __youtube_add_feed(self, ctx, name, id):
        self._add_feed(name,id)
        await self.__youtube_feeds(ctx)


    @commands.command()
    @commands.has_role("botadmin")
    async def youtube(self, ctx, *args):
        """
            Retrieves new youtube videos, and post the link in the designated channel.

            !youtube
                Shows the current configuration
            
            !youtube conf <channel> <role> <interval>
                Sets the configuration. The module check each <interval> in seconds if new videos are availble.
                If so, they are posted in <channel> and the mention <role> is used to notify the users interested.

            !youtube feeds
                Show the list of youtube feeds to retrieve
            
            !youtube feeds + <name> <id>
                Adds the youtube feed <id> with the custom <name> to the list of feeds to download
            
            !youtube feeds - <name>
                Removes the feed from the list

            !youtube downloads
                Shows the list of videos already downloaded
            
            !youtube downloads - <id>
                Removed the <id> video from the downloaded list.
            
            !youtube downloads clear
                Removes all the downloaded videos

        """
        if len(args) == 0:
            await self.__youtube(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'feeds':
                await self.__youtube_feeds(ctx)
                return
            if args[0] == 'downloads':
                await self.__youtube_downloads(ctx)
                return
        elif len(args) == 2:
            if args[0] == 'downloads' and args[1] == 'clear':
                await self.__youtube_clear(ctx)
                return
        elif len(args) == 3:
            if args[0] == 'feeds' and args[1] == '-':
                await self.__youtube_remove_feed(ctx, args[2])
                return
            if args[0] == 'downloads' and args[1] == '-':
                await self.__yourube_remove_download(ctx,args[2])
                return
        elif len(args) == 4:
            if args[0] == 'conf':
                await self.__youtube_conf(ctx,args[1],args[2],args[3])
                return
            if args[0] == 'feeds' and args[1] == '+':
                await self.__youtube_add_feed(ctx, args[2],args[3])
                return
        await ctx.send("🔔 Unrecognized command 🔔")


    @tasks.loop(seconds=1.0)
    async def get_youtube_feeds(self):
        try:
            logging.info("Starting get_youtube_feeds task")
            # Getting role for notifications
            role = discord.utils.get(self.bot.guilds[0].roles, name=self.config["role"])
            channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])

            feeds = self._get_feeds()
            for feed in feeds:
                url = "https://www.youtube.com/feeds/videos.xml?channel_id=" + feed
                rss = feedparser.parse(url)
                for video in rss.entries:
                    if not self._downloaded((video.id, feed)):
                        if (role == None):
                            await channel.send(video.link)
                        else:
                            await channel.send("\n" + role.mention + "\n" + video.link)
                        logging.info(f"New video posted: {video.title}")                        
                        self._save_feed((video.id,video.title, feed, video.published))
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")

    # Wait until bot is ready to retrieve youtube feeds     
    @get_youtube_feeds.before_loop
    async def before_get_youtube_feeds(self):
        logging.info("Waiting get_youtube_feeds to get ready")
        await self.bot.wait_until_ready()

# initialize plugin
def setup(bot):
	bot.add_cog(YouTubeFeeds(bot))

# Remove extension
def teardown(bot):
    logging.info("Removing Youtube-feeds")
    bot.remove_cog('youtube-feeds')
