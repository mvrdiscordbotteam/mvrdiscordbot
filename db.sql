BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "oculus-sales-games" (
	"id"	TEXT,
	"display_name"	TEXT,
	"imageURL"	TEXT,
	"expires"	INTEGER,
	"published"	INTEGER DEFAULT 0,
	"updated"	INTEGER,
	"promo"	TEXT
);
CREATE TABLE IF NOT EXISTS "oculus-sales" (
	"channel"	TEXT,
	"role"	TEXT,
	"interval"	REAL
);
CREATE TABLE IF NOT EXISTS "cal-events-list" (
	"uid"	TEXT,
	"title"	TEXT,
	"description"	TEXT,
	"date"	TEXT,
	"published"	INTEGER DEFAULT 0,
	"messageid"	TEXT,
	"image"	TEXT,
	"remainder"	INTEGER DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "cal-events" (
	"url"	TEXT,
	"interval"	REAL,
	"channel"	TEXT,
	"nrole"	TEXT,
	"rrole"	TEXT,
	"remainder1"	REAL,
	"remainder2"	REAL
);
CREATE TABLE IF NOT EXISTS "auto-role" (
	"channel"	TEXT,
	"admin"	TEXT
);
CREATE TABLE IF NOT EXISTS "auto-role-options" (
	"messageid"	INTEGER,
	"icon"	TEXT,
	"icontext"	TEXT,
	"action"	TEXT,
	"role"	TEXT
);
CREATE TABLE IF NOT EXISTS "auto-role-messages" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"messageid"	TEXT,
	"title"	TEXT,
	"text"	INTEGER
);
CREATE TABLE IF NOT EXISTS "badges" (
	"role"	TEXT,
	"icon"	TEXT
);
CREATE TABLE IF NOT EXISTS "youtube-feeds-downloaded" (
	"id"	TEXT,
	"title"	TEXT,
	"feed"	TEXT,
	"date"	TEXT
);
CREATE TABLE IF NOT EXISTS "youtube-feeds-list" (
	"idchannel"	TEXT,
	"description"	TEXT
);
CREATE TABLE IF NOT EXISTS "youtube-feeds" (
	"channel"	TEXT,
	"role"	TEXT,
	"interval"	REAL
);
CREATE TABLE IF NOT EXISTS "quest-releases-games" (
	"id"	TEXT,
	"display_name"	TEXT,
	"date"	TEXT
);
CREATE TABLE IF NOT EXISTS "quest-releases" (
	"channel"	TEXT,
	"role"	TEXT,
	"interval"	REAL
);
CREATE TABLE IF NOT EXISTS "go-releases-games" (
	"id"	TEXT,
	"display_name"	TEXT,
	"date"	TEXT
);
CREATE TABLE IF NOT EXISTS "go-releases" (
	"channel"	TEXT,
	"role"	TEXT,
	"interval"	REAL
);
CREATE TABLE IF NOT EXISTS "test" (
	"channel"	TEXT
);
CREATE TABLE IF NOT EXISTS "welcome-private" (
	"message"	TEXT
);
CREATE TABLE IF NOT EXISTS "welcome-counter" (
	"channel"	TEXT,
	"messagea"	TEXT,
	"messageb"	TEXT
);
CREATE TABLE IF NOT EXISTS "features" (
	"name"	TEXT,
	"active"	INTEGER DEFAULT 1,
	PRIMARY KEY("name")
);
COMMIT;
