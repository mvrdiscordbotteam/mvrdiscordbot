import logging 
from discord.ext import commands
import sys

class WelcomePrivate(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
        
    def _load_config(self):
       pass

    def _get_welcome_private(self):
        self.bot.database.cursor.execute('SELECT message FROM "welcome-private"')
        return  self.bot.database.cursor.fetchone()
    
    def _set_welcome_private(self, text):
        self.bot.database.cursor.execute('DELETE FROM "welcome-private"')
        self.bot.database.cursor.execute('INSERT INTO "welcome-private" (message) VALUES (?)',(text,))
        self.bot.database.c.commit()
    
    async def __welcome_private(self,ctx):
        msg = "__**Welcome Private message text**__\n"
        text = self._get_welcome_private()
        await ctx.send(msg + "\n " + text[0])

    async def __welcome_private_test(self,ctx):
        await self.welcome_user_pm(ctx.author)

    async def __welcome_private_conf(self,ctx, text):
        self._set_welcome_private(text)
        await self.__welcome_private_test(ctx)

    @commands.command()
    @commands.has_role("botadmin")
    async def welcome_private(self, ctx, *args):
        """
            Allows to send a custom private message to new members

            !welcome_private
                Shows the text of the message
            
            !welcome_private test
                Test the message sending a PM to the command issuer
            
            !welcome_private conf <new_message_text>
                Assign the new text to be used 

        """
        if len(args) == 0:
            await self.__welcome_private(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'test':
                await self.__welcome_private_test(ctx)
                return
        elif len(args) == 2:
            if args[0] == 'conf':
                await self.__welcome_private_conf(ctx,args[1])
                return
        await ctx.send("🔔 Unrecognized command 🔔")

    @commands.Cog.listener()
    async def on_member_join(self, member):
        try:
            logging.info(f'Welcoming PM to {member.name}')
            await self.welcome_user_pm(member)
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")


    async def welcome_user_pm(self, member):
        await member.create_dm()
        await member.dm_channel.send(self._get_welcome_private()[0])
        
# initialize plugin
def setup(bot):
	bot.add_cog(WelcomePrivate(bot))

# Remove extension
def teardown(bot):
    bot.remove_cog('welcome-private')
