import json
import logging
import sqlite3

from discord.ext import commands

# Dabase interface
class Database:
    def __init__(self, dburl):
        self.c = sqlite3.connect(dburl)
        #self.c.set_trace_callback(print)
        self.cursor = self.c.cursor()
    
    def get_active_feautures(self):
        self.cursor.execute("SELECT name FROM features WHERE active = 1")
        return self.cursor.fetchall()
    
    def get_all_feautures(self):
        self.cursor.execute("SELECT name FROM features")
        return self.cursor.fetchall()
    
# Load Configuration from config.json file
# token: Bot token
# prefix: Bot prefix
# database: URL to access database
class Configuration:
    def __init__(self):
        self.reload()
  
    def reload(self):
        with open('config.json') as f:
            self._config = json.load(f)
    
    @property
    def database(self):
        return self._config["databaseurl"]
    
    @property
    def prefix(self):
        return self._config["prefix"]

    @property
    def token(self):
        return self._config["token"]
    
    @property
    def loglevel(self):
        return self._config["logginglevel"]

    @property
    def botadminrole(self):
        return self._config["botadminrole"]

class CustomBot(commands.Bot): 
    def __init__(self, server_configuration):
        self.configuration = server_configuration
        self.database = Database(self.configuration.database)
        super().__init__(command_prefix=self.configuration.prefix)
        self.load_extension('kernel')
        self.load_features()
    
    def reload_features(self):
        self.unload_features()
        self.load_features()

    def unload_features(self):
        feautures = self.database.get_all_feautures()
        for feature in feautures:
            try:
                self.unload_extension(feature[0])
            except:
                logging.info(f"Extensión {feature[0]} was no loaded previously. !reload command")

    def load_features(self):
        feautures = self.database.get_active_feautures()
        logging.info(f"Reloading Bot features {feautures}")
        for feature in feautures:
            self.load_extension(feature[0])



# Main Program Entrance
if __name__ == "__main__":
    configuration = Configuration()
    logging.basicConfig(level=logging.getLevelName(configuration.loglevel),
                        format="%(levelname)s:%(module)s:%(lineno)d -  %(message)s")
    logging.info("Starting bot")
    bot = CustomBot(configuration)
    bot.run(configuration.token)



