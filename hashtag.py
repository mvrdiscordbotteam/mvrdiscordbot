import datetime
import discord
from discord.ext import commands
from discord.ext import tasks

import logging
import time
import re
import sys

class Hashtag(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
        self.check_old_hash_tags.change_interval(seconds=self.config["interval"])
        self.check_old_hash_tags.start()

    def cog_unload(self):
        self.check_old_hash_tags.cancel()
    
    def _load_config(self):
        self.bot.database.cursor.execute('SELECT interval,duration, channel FROM "hashtag"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["interval"] = tmp[0]
        self.config["duration"] = tmp[1]
        self.config["channel"] = tmp[2]

    def _tag_exists(self, tag):
        self.bot.database.cursor.execute('SELECT name FROM "hashtag-list" WHERE name = ?', (tag,))
        tmp = self.bot.database.cursor.fetchall()
        return len(tmp)
    
    def _save_tag(self, tag):
        self.bot.database.cursor.execute('INSERT INTO "hashtag-list" (name, date) VALUES (?,?)',(tag,str(time.time())))
        self.bot.database.c.commit()
    
    def update_tag_timestamp_database(self, tag):
        self.bot.database.cursor.execute('UPDATE "hashtag-list" SET date = ? WHERE name = ?',(str(time.time()),tag))
        self.bot.database.c.commit()
    
    def _get_old_hash_tags(self,time):
        self.bot.database.cursor.execute('SELECT name FROM "hashtag-list" WHERE date <  ? ',(time,))
        return self.bot.database.cursor.fetchall()
    
    def _remove_tag(self, tag):
        self.bot.database.cursor.execute('DELETE FROM "hashtag-list" WHERE name = ?', (tag,))
        self.bot.database.c.commit()
    
    def _get_conf(self):
        self.bot.database.cursor.execute('SELECT interval, duration, channel FROM "hashtag"')
        return self.bot.database.cursor.fetchone()
    
    def _set_conf(self,inteval, duration, channel):
        self.bot.database.cursor.execute('DELETE FROM  "hashtag"')
        self.bot.database.cursor.execute('INSERT INTO  "hashtag" (interval, duration, channel) VALUES (?,?,?)',(inteval,duration,channel))
        self.bot.database.c.commit()
    
    def _get_tags(self):
        self.bot.database.cursor.execute('SELECT name, date FROM "hashtag-list" order by date')
        return self.bot.database.cursor.fetchall()
    
    async def __hashtag(self, ctx):
        conf = self._get_conf() 
        msg = "__**Hashtag configuration**__\n"
        await ctx.send(msg + "\n**Channel**: " + conf[2] + "\n**Hashtag lifetime**: " + str(conf[1]) + " seconds\n**Perimed hashtag checking interval**: " + str(conf[0]) + " seconds")
            
    async def __hashtag_list(self, ctx):
        list = self._get_tags()
        msg = "__**List of Hashtags**__\n"
        for i in list:
            t = float(i[1]) - (time.time() - int(self.config["duration"])) 
            msg += "\n**" + i[0] + "** expires in " + str(int(t)) + " seconds"
        await ctx.send(msg)

    async def __hashtag_clean(self, ctx):
        roles = self.bot.guilds[0].roles
        for role in roles:
            name = role.name
            if name.startswith("#"):
                if not self._tag_exists(name[1:]):
                    try:
                        await role.delete(reason="Hashtag perimed tag bot")            
                    except:
                        pass
                    await ctx.send(f"Deleting role {name}")
        

    async def __hashtag_list_remove(self,ctx, hash):
        role = discord.utils.get(self.bot.guilds[0].roles, name="#" + hash)
        try:
            await role.delete(reason="Hashtag perimed tag bot")            
        except:
            pass
        self._remove_tag(hash)
        await ctx.send("\n hashtag **#" + hash + "** removed")

    async def __hashtag_conf(self, ctx, inteval, duration, channel):
        self._set_conf(inteval, duration, channel)
        await self.__hashtag(ctx)
        await ctx.send("\nRemember to !reload to apply new configuration")

    @tasks.loop(seconds=1.0)
    async def check_old_hash_tags(self):
        try:
            oldtags = self._get_old_hash_tags(str(time.time() - int(self.config["duration"])))
            #tagicon = discord.utils.get(self.bot.guilds[0].emojis,name="tag")
            tagicon = "#⃣"
            channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
            for otag in oldtags:
                    role = discord.utils.get(self.bot.guilds[0].roles, name="#" + otag[0])
                    embedmessage = discord.Embed()
                    embedmessage.title = str(tagicon) + " Hashtag removed " + str(tagicon)
                    embedmessage.colour = discord.Colour.blue()
                    msg = "** **\nThe mention __**#" + str(otag[0]) + "**__ has been removed due to lack of use"
                    embedmessage.description = msg
                    await channel.send("\n" + role.mention + "\n", embed=embedmessage)
                    try:
                        await role.delete(reason="Hashtag perimed tag bot")            
                    except:
                        pass
                    self._remove_tag(otag[0])
                    logging.info(f'Hastag #{otag[0]} removed by expiration')

        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")

     # Wait until bot is ready to remove old hashtags    
    @check_old_hash_tags.before_loop
    async def before_check_old_hash_tags(self):
        logging.info("Waiting check_old_hash_tags to get ready")
        await self.bot.wait_until_ready()

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        try:	
            if payload.user_id == self.bot.user.id:
                return # Bot itself reactions are ignored
            #if payload.emoji.name != "tag":
            if payload.emoji.name != "#⃣" and payload.emoji.name != "#️⃣":
                return
            channel = self.bot.guilds[0].get_channel(payload.channel_id)
            message = await channel.fetch_message(payload.message_id)
            mentions = message.role_mentions
            addedmentions = ""
            member = self.bot.guilds[0].get_member(payload.user_id)
            for mention in mentions:
                if mention.name.startswith("#"):  
                    await member.add_roles(discord.Object(mention.id), reason="HashTag module")
                    addedmentions += mention.name + " "

            if addedmentions != "":
                await member.create_dm()
                #tagicon = discord.utils.get(self.bot.guilds[0].emojis,name="tag")
                tagicon = "#⃣"
                msg = "You have been correctly subscribed to **__" + addedmentions + "__** tag.\n"
                msg += "Remember you can unsubscribe from the tag removing the special " + str(tagicon) + " hashtag icon reaction"
                await member.dm_channel.send(msg)
                logging.info(f'User {member.name} has subscribed to {addedmentions}')
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")


    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        try:	
            if payload.user_id == self.bot.user.id:
                return # Bot itself reactions are ignored
            #if payload.emoji.name != "tag":
            if payload.emoji.name != "#⃣"  and payload.emoji.name != "#️⃣":
                return
            channel = self.bot.guilds[0].get_channel(payload.channel_id)
            message = await channel.fetch_message(payload.message_id)
            mentions = message.role_mentions
            addedmentions = ""
            member = self.bot.guilds[0].get_member(payload.user_id)
            for mention in mentions:
                if mention.name.startswith("#"):  
                    await member.remove_roles(discord.Object(mention.id), reason="HashTag module")
                    addedmentions += mention.name + " "

            if addedmentions != "":
                await member.create_dm()
                #tagicon = discord.utils.get(self.bot.guilds[0].emojis,name="tag")
                tagicon = "#⃣"
                msg = "You have been correctly unsubscribed the **__" + addedmentions + "__** tags.\n"
                await member.dm_channel.send(msg)
                logging.info(f'User {member.name} has UNsubscribed to {addedmentions}')

        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")


    @commands.Cog.listener()
    async def on_message(self, message):
        try:
            if message.author.id == self.bot.user.id:
                return # Bot itself reactions are ignored
            self.update_tag_timestamp(message)
            #m = re.search('<:tag:\d*>(\s*\w*)', message.content)
            m = re.search('(#️⃣|#⃣)(\s*\w*)', message.content)
            logging.info(m)
            if m:
                tag = m.group(2)
                if tag[1] == " ":
                    return
                tag = tag.strip()
                if self._tag_exists(tag):
                    role = discord.utils.get(self.bot.guilds[0].roles, name="#" + tag)
                    await message.author.add_roles(discord.Object(role.id), reason="HashTag module")
                    embedmessage = discord.Embed()
                    #tagicon = discord.utils.get(self.bot.guilds[0].emojis,name="tag")
                    tagicon = "#⃣"
                    embedmessage.title = str(tagicon) + " Hashtag was already in use" + str(tagicon)
                    embedmessage.colour = discord.Colour.blue()
                    msg = "** **\nThe mention" + role.mention + " was already created previosly\n\n"
                    msg += message.author.mention + " You have been added to follow the hashtag"
                    embedmessage.description = msg
                    await message.channel.send(embed=embedmessage)
                    logging.info(f'User {message.author.name} has subscribed to {role.name} AFTER REPEATING TAG')
                else:
                # Create Role
                    role = await self.bot.guilds[0].create_role(name="#" + tag, colour=discord.Colour.blue(),mentionable=True,reason="Bot hastag")
                    await message.author.add_roles(discord.Object(role.id), reason="HashTag module")
                    self._save_tag(tag)
                    embedmessage = discord.Embed()
                    #tagicon = discord.utils.get(self.bot.guilds[0].emojis,name="tag")
                    tagicon = "#⃣"
                    embedmessage.title = str(tagicon) + " New hashtag mention created " + str(tagicon)
                    embedmessage.colour = discord.Colour.blue()
                    msg = "** **\nThe mention" + role.mention + " has been created\n\nYou can use this __mention__ to continue talking about this topic."
                    msg += "\n\nIf any user wants to follow the **__#" + tag + "__** topic, they can react with the special hashtag icon " + str(tagicon) + " in any message this mention is used"
                    embedmessage.description = msg
                    await message.channel.send("\n" + role.mention,embed=embedmessage)
                    logging.info(f'User {message.author.name} has subscribed to {role.name} NEW TAG')
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")

    @commands.command()
    @commands.has_role("botadmin")
    async def hashtag(self, ctx, *args):
        """
            Allows to create mention roles based in special character "#⃣" 

            !hashtag
                Shows the current configuration
            
            !hashtag conf <interval> <duration> <channel>
                Sets how long in <duration> seconds, the hashtag is valid after last use. Hashtag are checked
                if they are perimed each <interval> seconds. If so, a message is posted in <channel> to advertise
                the hashtag has finished

            !hashtag list
                Show the available hashtags
            
            !hashtag list - <hashname>
                Removes the hashtag 
            
            !hashtag list clean
                Removes roles with # character that is not in the tag list for cleaning

        """
        if len(args) == 0:
            await self.__hashtag(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'list':
                await self.__hashtag_list(ctx)
                return
        elif len(args) == 2:
            if args[0] == 'list' and args[1] == 'clean':
                await self.__hashtag_clean(ctx)
                return
        elif len(args) == 3:
            if args[0] == 'list' and args[1] == '-':
                await self.__hashtag_list_remove(ctx, args[2])
                return
        elif len(args) == 4:
            if args[0] == 'conf':
                await self.__hashtag_conf(ctx,args[1],args[2],args[3])
                return
        await ctx.send("🔔 Unrecognized command 🔔")


    def update_tag_timestamp(self, message):
        mentions = message.role_mentions
        for mention in mentions:
            if mention.name.startswith("#"):
                tag = mention.name[1:] 
                if self._tag_exists(tag):
                    self.update_tag_timestamp_database(tag)


# initialize plugin
def setup(bot):
	bot.add_cog(Hashtag(bot))
# Remove extension
def teardown(bot):
    bot.remove_cog('hashtag')
