import feedparser
from discord.ext import tasks
import discord.utils
from discord.ext import commands
import logging
import sys


class UploadNews(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
        self.get_upload_news.change_interval(seconds=self.config["interval"])
        self.get_upload_news.start()
    
    def cog_unload(self):
        self.get_upload_news.cancel()

    def _load_config(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "upload-news"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["channel"] = tmp[0]
        self.config["role"] = tmp[1]
        self.config["interval"] = tmp[2]
    
    def _get_feeds(self):
        self.bot.database.cursor.execute('SELECT urlrss, filter,name FROM "upload-news-feeds"')
        feeds = self.bot.database.cursor.fetchall()
        return feeds

    def _downloaded(self, link):
        self.bot.database.cursor.execute('SELECT link FROM "upload-news-downloaded" WHERE link = ?', (link,))
        tmp = self.bot.database.cursor.fetchall()
        return len(tmp)
    
    def _save_feed(self, link):
        self.bot.database.cursor.execute('INSERT INTO "upload-news-downloaded" (link) VALUES (?)', (link,))
        self.bot.database.c.commit()
    
    def _get_conf(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "upload-news"')
        return self.bot.database.cursor.fetchone()
    
    def _set_conf(self,channel, role, interval):
        self.bot.database.cursor.execute('DELETE FROM  "upload-news"')
        self.bot.database.cursor.execute('INSERT INTO  "upload-news" (channel, role, interval) VALUES (?,?,?)',(channel,role,interval))
        self.bot.database.c.commit()
    
    def _clean_all(self):
        self.bot.database.cursor.execute('DELETE FROM  "upload-news-downloaded"')
        self.bot.database.c.commit()
    
    def _insert_new_feed(self,name,url,filter):
        self.bot.database.cursor.execute('INSERT INTO  "upload-news-feeds" (urlrss, filter,name) VALUES (?,?,?)',(url,filter,name))
        self.bot.database.c.commit()
    
    def _remove_feed(self,name):
        self.bot.database.cursor.execute('DELETE FROM "upload-news-feeds"  WHERE name = ?',(name,))
        self.bot.database.c.commit()
        
    async def __rssnews(self,ctx):
        conf = self._get_conf() 
        msg = "__**RSS news configuration**__\n"
        await ctx.send(msg + "\n**Channel**: " + conf[0] + "\n**Role for alerts:**: " + conf[1] + "\n**Retrieval interval**: " + str(conf[2]) + " seconds")
    
    async def __rssnews_feeds(self, ctx):
        list = self._get_feeds()
        msg = "__**List of Feeds**__\n"
        await ctx.send(msg)
        for i in list:
            await ctx.send("\n" + i[2] + " - [" + str(i[1]) + "] - " + i[0])
    
    async def __rssnews_clean(self, ctx):
        self._clean_all()
        msg = "\n🔔 All news removed 🔔\n"
        await ctx.send(msg)
    
    async def __rssnews_feed_remove(self,ctx,name):
        self._remove_feed(name)
        await self.__rssnews_feeds(ctx)
    
    async def __rssnews_conf(self,ctx,channel,role,interval):
        self._set_conf(channel,role, interval)
        self._load_config()
        await self.__rssnews(ctx)

    async def __rssnews_feed_add(self, ctx, name,url,filter):
        self._insert_new_feed(name,url,filter)
        await self.__rssnews_feeds(ctx)

    @commands.command()
    @commands.has_role("botadmin")
    async def rssnews(self, ctx, *args):
        """
            Allows to retrieve news from RSS or RSSv2 feeds filtered by key words

            !rssnews
                Shows the current configuration
            
            !rssnews conf <channel> <role> <interval>
                Sets the <channel> to post the new feeds, with mention <role> for alerts, and the frecuency <interval> in seconds RSS are checked

            !rssnews feeds
                Show the feeds to check
            
            !rssnews feeds + <name> <urlrss> <filter>
                Add a feed with <name> and accessed by <urlrss> and filtered by <filter> (comma separated values) 
            
            !rssnews feeds - <name>
                Removes the feed named <name>
            
            !rssnews feeds clean
                Removed the downloaded news

        """
        if len(args) == 0:
            await self.__rssnews(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'feeds':
                await self.__rssnews_feeds(ctx)
                return
        elif len(args) == 2:
            if args[0] == 'feeds' and args[1] == 'clean':
                await self.__rssnews_clean(ctx)
                return
        elif len(args) == 3:
            if args[0] == 'feeds' and args[1] == '-':
                await self.__rssnews_feed_remove(ctx, args[2])
                return
        elif len(args) == 4:
            if args[0] == 'conf':
                await self.__rssnews_conf(ctx,args[1],args[2],args[3])
                return
        elif len(args) == 5:
            if args[0] == "feeds" and args[1] == '+':
                await self.__rssnews_feed_add(ctx,args[2],args[3],args[4])
                return
        await ctx.send("🔔 Unrecognized command 🔔")




    @tasks.loop(seconds=1.0)
    async def get_upload_news(self):
        logging.info("Starting get_upload_news task")
        try:
            role = discord.utils.get(self.bot.guilds[0].roles, name=self.config["role"])
            channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])

            feeds = self._get_feeds()
            for feed in feeds:
                # feed[0] is the feed URL
                rss = feedparser.parse(feed[0])
                # feed[1] is the filter list of word
                # We store in tags the worlds to filter
                if feed[1]:
                    tags = feed[1].split(',')
                else:
                    tags = None
                # isTarget determines if the news matches the filter
                isTarget = False
                for new in rss.entries:
                    # First we filter the new with tags words
                    if tags:
                        for tag in new.tags:
                            if tag.term in tags:
                                isTarget = True
                                break
                    else:
                        isTarget = True
                    # If it matches the critera we publish it
                    if isTarget:
                        if not self._downloaded(new.link):
                            await channel.send("\n" + role.mention + "\n" + new.link)
                            self._save_feed(new.link)
                            logging.info(f"New new posted: {new.link}")                        
                    isTarget = False
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")
                
    @get_upload_news.before_loop
    async def before_get_upload_news(self):
        logging.info("Waiting get_upload_news to get ready")
        await self.bot.wait_until_ready()

# initialize plugin
def setup(bot):
	bot.add_cog(UploadNews(bot))
# Remove extension
def teardown(bot):
    bot.remove_cog('upload-news')
