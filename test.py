import datetime
import discord
from discord.ext import commands
import logging
import asyncio

class Test(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
    
    def _load_config(self):
        self.bot.database.cursor.execute('SELECT channel FROM "test"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["channel"] = tmp[0]
   
    @commands.command()
    @commands.has_role("botadmin")
    async def test(self, ctx):
        """
            Print a test string    
        """
        logging.info(f'{self.bot.user.name} has connected to Discord')
        await ctx.send(f"Hola {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}")
    
    @commands.Cog.listener()
    async def on_message(self, message):
        print(message.content)

# initialize plugin
def setup(bot):
	bot.add_cog(Test(bot))

# Remove extension
def teardown(bot):
    logging.info("Removing test")
    bot.remove_cog('test')
