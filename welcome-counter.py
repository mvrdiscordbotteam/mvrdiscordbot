from discord.ext import commands
import discord
import logging
import sys


class WelcomeCounter(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
    
    def _load_config(self):
        self.bot.database.cursor.execute('SELECT channel, messagea FROM "welcome-counter"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["channel"] = tmp[0]
        self.config["messagea"] = tmp[1]
        self.config["messageb"] = "You are our member number "

    def _get_welcome_counter_conf(self):
        self.bot.database.cursor.execute('SELECT channel, messagea FROM "welcome-counter"')
        return self.bot.database.cursor.fetchone()

    def _set_welcome_counter(self, channel, message):
        self.bot.database.cursor.execute('DELETE FROM "welcome-counter"')
        self.bot.database.cursor.execute('INSERT INTO "welcome-counter" (channel, messagea) VALUES (?,?)',(channel,message))
        self.bot.database.c.commit()

    async def __welcome_counter(self,ctx):
        msg = "__**Welcome Counter configuration**__\n"
        conf = self._get_welcome_counter_conf()
        await ctx.send(msg + "\n**Channel**: " + conf[0] + "\n**Message**: " + conf[1])

    async def __welcome_counter_test(self,ctx):
        await self.show_counter(ctx.author, ctx.channel)

    async def __welcome_counter_conf(self,ctx, channel, message):
        self._set_welcome_counter(channel,message)
        self._load_config()
        await self.__welcome_counter_test(ctx)

    @commands.command()
    @commands.has_role("botadmin")
    async def welcome_counter(self, ctx, *args):
        """
            Welcome a newcomer in the designated <channel> with the <message> and the member counter

            !welcome_counter
                Shows the configuration
            
            !welcome_counter conf <channel> <message>
                Sets the welcoming <message>, and the <channel> where the message will be posted
            
            !welcome_counter test
                Test the welcoming message with the command issuer

        """
        if len(args) == 0:
            await self.__welcome_counter(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'test':
                await self.__welcome_counter_test(ctx)
                return
        elif len(args) == 3:
            if args[0] == 'conf':
                await self.__welcome_counter_conf(ctx,args[1], args[2])
                return
        await ctx.send("🔔 Unrecognized command 🔔")

    @commands.Cog.listener()
    async def on_member_join(self, member):
        try:
            logging.info(f'Welcoming counter to new member {member.name}')
            await self.show_counter(member,discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"]))
        except Exception as e:
             logging.error(f"Error {e}: {sys.exc_info()[0]}")

    async def show_counter(self, member, channel):
        message = f'{self.config["messagea"]} \n\n👋 **{member.name}** 👋\n\n{self.config["messageb"]}**{str(self.bot.guilds[0].member_count)}**'
        await channel.send(message)

# initialize plugin
def setup(bot):
	bot.add_cog(WelcomeCounter(bot))

# Remove extension
def teardown(bot):
    bot.remove_cog('welcome-counter')

