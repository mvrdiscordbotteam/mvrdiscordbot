from discord.ext import commands
import logging
import sys

class Badges(commands.Cog):
	def __init__(self,bot):
		self.bot = bot
		self.config = {}

	def _get_badges(self):
		self.bot.database.cursor.execute('SELECT role, icon FROM "badges"')
		return  self.bot.database.cursor.fetchall()
	
	def _badges_add(self,role,icon):
		self.bot.database.cursor.execute('INSERT INTO "badges" (role, icon) VALUES (?,?)',(role,icon))
		self.bot.database.c.commit()

	def _badges_remove(self,role):
		self.bot.database.cursor.execute('DELETE FROM  "badges"  WHERE role = ?',(role,))
		self.bot.database.c.commit()


	async def __badges(self,ctx):
		msg = "__**Badges list**__\n"
		badges = self._get_badges()
		for badge in badges:
			msg += "\n" + badge[1] + " " + badge[0]
		await ctx.send(msg)

	async def __badges_add(self,ctx,role,icon):
		try:
			self._badges_add(role,icon)
			await self.__badges(ctx)
		except Exception as e:
			await ctx.send(f"Error {e}")

	async def __badges_remove(self,ctx,role):
		try:
			self._badges_remove(role)
			await self.__badges(ctx)
		except Exception as e:
			await ctx.send(f"Error {e}")


	async def __badges_reload(self,ctx):
		res = ""
		members = self.bot.guilds[0].members
		for member in members:
			badgeroles = self._get_badges()
			newname = member.name
			for badgrole in badgeroles:
				for role in member.roles:
					if badgrole[0] == role.name:
						newname += " " + badgrole[1]
						await ctx.send("\n" + newname)
			try:
				await member.edit(nick=newname,reason="Badges module")
			except:
				logging.error(f"⛔ Error assigning badge to {member.name}")
				await ctx.send("\n⛔ Error assigning badge to " + member.name)
		
	@commands.Cog.listener()
	async def on_member_update(self, before,after):
		try:
			if (before.roles != after.roles):
				badgeroles = self._get_badges()
				newname = before.name
				for badgrole in badgeroles:
					for role in after.roles:
						if badgrole[0] == role.name:
							newname += " " + badgrole[1]
				await after.edit(nick=newname,reason="Badges module")
				logging.info(f"Changed nick name with badges to {newname}")
		except Exception as e:
			logging.error(f"Error {e}: {sys.exc_info()[0]}")

	@commands.command()
	@commands.has_role("botadmin")
	async def badges(self, ctx, *args):
		"""
			Allows to assign a icon to user nicknames based on role membership

			!badges
				Shows the badges available
			
			!badges + <role> <icon>
				Creates a new badge assigning an icon to a role
			
			!badges - <role>
				Removes the Badge

			!badges reload
				Recalculates and asigns all user badges

		"""
		if len(args) == 0:
			await self.__badges(ctx)
			return
		elif len(args) == 1:
			if args[0] == 'reload':
				await self.__badges_reload(ctx)
				return
		elif len(args) == 3:
			if args[0] == '+':
				await self.__badges_add(ctx,args[1],args[2])
				return
		elif len(args) == 2:
			if args[0] == '-':
				await self.__badges_remove(ctx,args[1])
				return
		await ctx.send("🔔 Unrecognized command 🔔")

# initialize plugin
def setup(bot):
	bot.add_cog(Badges(bot))
# Remove extension
def teardown(bot):
    bot.remove_cog('badges')
