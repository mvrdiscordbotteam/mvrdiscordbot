

## Features list to explore/test

* [X] Auto welcoming message to newcomers, with/whitout mention, and server membership counter
* [X] Auto private welcoming message to newcomers with server instructions 
* [X] Auto posting Youtube RSS feeds im any channel designed
* [X] Auto posting new Go releases on the store 
* [X] Auto posting new Quest release on the store
* [X] Blog/Media RSS feeds retrieval with category filter 
* [X] Role selection by reaction icon in welcoming channel 
* [X] Name badges based on role membership
* [X] Google calendar sync in scheduled events
* [X] Party private message remander is user reacted to icon in even
* [X] Hastag notification
* [ ] Devs $ games database
* [X] Sales tracking (new sales available daily different from the last update)
* [ ] Server stats like total active users today" or even just "Total Member count". Maybe even "Posts Made Today"

## Technical work
* [ ] Filter bot execution to designed GUILD id in config file
* [ ] Exception management
* [ ] Web frontend for bot configuration 
* [ ] Backend for bot configuration ¿REST? service

### Auto welcoming message to newcomers, with/whitout mention, and server membership counter [welcome-counter]()

When a user joins the server a predefined message in the form **messagea** @user_mention* **messageb** <server_user_counter>
is posted in **channelid**

**messagea** *str* -> First part messsage
**messagab** *str* -> Second part message after user mention
**channelid** *int* -> Channel to post the welcoming message

### Auto private welcoming message to newcomers with server instructions [welcome-private]()

When a user joins the server a predefined private **message** is sent to the user.

**message** *str* -> Private welcoming message

### Auto posting Youtube RSS feeds im any channel designed with category filter [youtube-feeds]()

Retrieve periodically RSS youtube **feeds** for new videos and post their URL in the designed **channelid**

**feeds** *array* -> Array of RSS youtube ids
**channelid** *int* -> Channel where to post youtube videos

### Blog/Media RSS feeds retrieval with category filter [blog-rss]()

Automatic posting of links to new articles from RSS **feeds** with **name** retrieved from **url** that contains the **keywords** list designed

**feed** *array* -> Array of feeds objects
    **name** *str* -> Name of the blog/page source
    **url** *str* -> URL to get the feed
    **keywords** *array* -> Keyword list to filter message in category field in RSS feed

### Role selection by reaction icon in #welcoming channel [auto-role]()

Predefined array of **messages** idenfied by **messageid** composed of **text** and **options** for a reaction menu published in designed **channelid** 

At start, bot will create the messages, if not available their **messageid** in the the channel **channelid** .

Each message is composed of **text**, and **options**

Each **option** is composed of **icon**, **icontext**, **trigger** 

At the end, a reaction list of icons (corresponding to each option) will be showed. User can only react to the predefined icon list (other reaction should be removed).User can react/unreact to all the options 
When the user press a reaction icon the **trigger** is launched

**action** can be:
- Auto Role asignment. Composed of "arole" keyword and *role* name to use
    In this case. the role is automatic asigned/removed to the user
- Role proposal. Composed of *prole* keyword and *role* name to use (for dev/content creator roles )
    In this case a private message to admins is sent indicating the user request and a private message is sent to the user indicating waiting for admins to aprove

**channelid** *int* -> Channel where to post selection
**messages** *array* -> Array of messages for autorole asignment
    **messageid** *int* -> Id of the message for reaction tracking
    **text** *str* -> Text previos to the options
    **options** *array* -> Array of options for each message
        **icon** *str* -> Icon to use for reaction
        **icontext** *str* -> Short description for icon reaction meaning
        **trigger** *obj* -> Action to do after user press reaction icon
            **action** *str* -> Action to do [arole] for automatic role, [prole] for proposal role
            **role** *str* -> Role afected
        
### Name badges based on role membership [badges]()
Each time a guild member is updated (role updated), the user name is 'calculated' adding a certain list of **icon** at the username (as **badges**) trailing his name representing the roles he has in the server 

Expample. User is call 'Sam' --> it becomes 'Sam :go: :quest: :dev:'

**badges** *array* -> List of badges to calculate. Badges order is important for icon orde
    **icon** *str* -> Icon or custom icon to add
    **role** *str* -> Add the **icon** is the user has this **role**

### Google calendar sync in scheduled events
Philips maintains a public Google Calendar for the events. His owns a webpage that reads from that Calendar.
The bot should read the Calendar events (two week time frame) and update the **channelid* with the events on the calendar

### Party private message remander is user reacted to icon in even
When a user reacts to an event icon, is added to the list for an automic private message when the event is due in predefined time

### Hastag notification
Test a way to subscribe to task a receive a notitication when is used

### Devs & games database
Database of games, and devs users in the discord for contact

### Sales tracking (new sales available daily different from the last update)
Post sales different from las update. Update message periodically   