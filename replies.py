import datetime
import discord
from discord.ext import commands
import logging
import re
import sys

class Replies(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
    
    def _load_config(self):
        self.bot.database.cursor.execute('SELECT icon FROM "replies"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["icon"] = tmp[0]
    
    def _save_entry(self, msg, user, channel):
        self.bot.database.cursor.execute('INSERT INTO "replies-list" (message,user,channel) VALUES (?,?,?)', (msg, user,channel))
        self.bot.database.c.commit()
    
    def _remove_entry(self, msg, user, channel):
        self.bot.database.cursor.execute('DELETE FROM "replies-list" WHERE message = ?  AND user = ? AND channel = ?', (msg, user,channel))
        self.bot.database.c.commit()
    
    def _any_pending_reply_from_user(self, userid):
        self.bot.database.cursor.execute('SELECT message, user, channel FROM "replies-list" WHERE user = ?',(userid,))
        return self.bot.database.cursor.fetchone()
    
    def _remove_message(self, user, msgid):
        self.bot.database.cursor.execute('DELETE FROM "replies-list" WHERE user = ? AND message = ?',(user,msgid))
        self.bot.database.c.commit()

    def _get_conf(self):
        self.bot.database.cursor.execute('SELECT icon FROM "replies"')
        tmp = self.bot.database.cursor.fetchone()
        return tmp

    def _get_replies(self):
        self.bot.database.cursor.execute('SELECT message, user, channel FROM "replies-list" order by user')
        return self.bot.database.cursor.fetchall()
    
    def _clean_replies(self):
        self.bot.database.cursor.execute('DELETE FROM "replies-list"')
        self.bot.database.c.commit()
    
    def _set_conf(self, icon):
        self.bot.database.cursor.execute('DELETE FROM  "replies"')
        self.bot.database.cursor.execute('INSERT INTO  "replies" (icon) VALUES (?)',(icon,))
        self.bot.database.c.commit()

    async def __replies(self, ctx):
        conf = self._get_conf() 
        msg = "__**Replies configuration**__\n"
        tagicon = discord.utils.get(self.bot.guilds[0].emojis,name=conf[0])
        await ctx.send(msg + "\n**Custom Icon Name**: " + conf[0] + "\nrepresented as " + str(tagicon))

    async def __replies_list(self, ctx):
        replies = self._get_replies()
        msg = "__**Pending replies**__\n"
        await ctx.send(msg)
        for repl in replies:
            user = self.bot.guilds[0].get_member(int(repl[1]))
            message = repl[0]
            channel = self.bot.guilds[0].get_channel(int(repl[2]))
            await ctx.send("**" + user.name + "** - " + message + " - (" + channel.name + ")")
    
    async def __replies_clean(self, ctx):
        self._clean_replies()
        await self.__replies_list(ctx)
    
    async def __replies_set_conf(self,ctx,icon):
        self._set_conf(icon)
        self._load_config()
        await self.__replies(ctx)
    
    @commands.command()
    @commands.has_role("botadmin")
    async def replies(self, ctx, *args):
        """
            Allows to create a answer thread by means of reacting with a specific icon

            !replies
                Shows the current configuration
            
            !replies conf <custom_icon_name>
                Sets the <custom_icon_name> to use in the replies

            !replies list
                Show the pending replies
        
            !replies list clean
                Removes all pending replies

        """
        if len(args) == 0:
            await self.__replies(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'list':
                await self.__replies_list(ctx)
                return
        elif len(args) == 2:
            if args[0] == 'list' and args[1] == 'clean':
                await self.__replies_clean(ctx)
                return
            elif args[0] == 'conf':
                await self.__replies_set_conf(ctx,args[1])
        await ctx.send("🔔 Unrecognized command 🔔")



    @commands.Cog.listener()
    async def on_message(self, message):
        try:
            if message.author.id == self.bot.user.id:
                return # Bot itself reactions are ignored
            repliefrom = self._any_pending_reply_from_user(message.author.id)
            if repliefrom:
                # Get original channel where the message was posted
                channel = self.bot.guilds[0].get_channel(int(repliefrom[2]))
                msg = await channel.fetch_message(int(repliefrom[0]))
                # oser = Original user of the first message
                ouser = msg.author
                # if the reply is from the bot, we have to check the original author in the REPLY text message
                if ouser.bot:
                    embed = msg.embeds[0]
                    if embed:
                        if embed.title.startswith("REPLY"):
                            omember = self.bot.guilds[0].get_member_named(embed.author.name)           
                            ouser = omember
                embedmessage = discord.Embed()
                if ouser.nick:
                    embedmessage.title = "REPLY to **" + ouser.nick + "** __message__"
                else:
                    embedmessage.title = "REPLY to **" + ouser.name + "** __message__"
                if (message.author.nick):
                    embedmessage.set_author(name=message.author.nick ,url=message.jump_url,icon_url=message.author.avatar_url)
                else:
                    embedmessage.set_author(name=message.author.name ,url=message.jump_url,icon_url=message.author.avatar_url)
                embedmessage.colour = discord.Colour.light_grey()
                embedmessage.url = msg.jump_url
                embedmessage.set_thumbnail(url=message.author.avatar_url)
                embedmessage.description = msg.content[:200] + "\n** **\n"
                if ouser.nick:
                    embedmessage.set_footer(text=ouser.nick + msg.created_at.strftime(" 📆 %d %b %H:%M UTC"),icon_url=ouser.avatar_url)
                else:
                    embedmessage.set_footer(text=ouser.name + msg.created_at.strftime(" 📆 %d %b %H:%M UTC"),icon_url=ouser.avatar_url)
                content = message.content  + "\n" +  ouser.mention
                await message.channel.send(content=content ,embed=embedmessage,)
                await message.delete()
                self._remove_message(message.author.id,msg.id)
                logging.info(f'New reply from {ouser.name} to {message.author.name}')
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")


    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):	
        if payload.user_id == self.bot.user.id:
            return # Bot itself reactions are ignored
        if  payload.emoji.name != self.config["icon"]:
            return
        self._save_entry(payload.message_id,  payload.user_id, payload.channel_id)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        if payload.user_id == self.bot.user.id:
            return # Bot itself reactions are ignored
        if  payload.emoji.name != self.config["icon"]:
            return
        self._remove_entry(payload.message_id,  payload.user_id, payload.channel_id)

# initialize plugin
def setup(bot):
	bot.add_cog(Replies(bot))
# Remove extension
def teardown(bot):
    bot.remove_cog('replies')
