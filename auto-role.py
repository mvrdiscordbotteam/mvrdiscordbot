import asyncio
import datetime
import emoji
import json
import requests
import time
import unicodedata
import discord
from discord.ext import commands
import feedparser
from datetime import timedelta
import logging
import sys

class AutoRole(commands.Cog):
	def __init__(self, bot):
		self.bot = bot # initialize bot
		self.config = {}
		self._load_config()
	
	def _load_config(self):
		self.bot.database.cursor.execute('SELECT channel, admin FROM "auto-role"')
		tmp = self.bot.database.cursor.fetchone()
		self.config["channel"] = tmp[0]
		self.config["admin"] = tmp[1]
		self.bot.database.cursor.execute('SELECT id, messageid, title, text FROM "auto-role-messages"')
		self.config["messages"] = []
		tmp = self.bot.database.cursor.fetchall()
		
		for message in tmp:
			m = {}
			m["id"] = message[0]
			m["messageid"] = message[1]
			m["title"] = message[2]
			m["text"] = message[3]
			self.config["messages"].append(m)

		for message in self.config["messages"]:
			
			self.bot.database.cursor.execute('SELECT icon, icontext, action, role FROM "auto-role-options" WHERE messageid = ?', ( message["id"], ))
			message["options"] = []
			tmp = self.bot.database.cursor.fetchall()
			for option in tmp:
				o = {}
				o["icon"] = option[0]
				o["icontext"] = option[1]
				o["action"] = option[2]
				o["role"] = option[3]
				message["options"].append(o)
	
	def _get_messages(self):
		self.bot.database.cursor.execute('SELECT id, messageid, title, text, channel FROM "auto-role-messages"')
		return self.bot.database.cursor.fetchall() 

	def _save_message_id(self, message):
		self.bot.database.cursor.execute('UPDATE "auto-role-messages" SET  messageid = ?  WHERE id = ? ', message)
		self.bot.database.c.commit()
	
	def _get_message_options(self, message):
		self.bot.database.cursor.execute('SELECT icon, icontext, action, role, am.messageid FROM "auto-role-options" as op LEFT JOIN "auto-role-messages" as am ON op.messageid = am.id WHERE am.messageid = ?', message)
		return self.bot.database.cursor.fetchall()
	
	def _get_message_options_by_id(self, message):
		self.bot.database.cursor.execute('SELECT icon, icontext, action, role, am.messageid FROM "auto-role-options" as op LEFT JOIN "auto-role-messages" as am ON op.messageid = am.id WHERE am.id = ?', message)
		return self.bot.database.cursor.fetchall()

	def _check_message_belongs_to_autorole(self, messageid):
		self.bot.database.cursor.execute('SELECT messageid FROM "auto-role-messages" WHERE messageid = ?',(messageid,))
		tmp = self.bot.database.cursor.fetchall()
		if len(tmp) > 0:
			return True
		else:
			return False

	def _get_conf(self):
		self.bot.database.cursor.execute('SELECT channel, admin FROM "auto-role"')
		return self.bot.database.cursor.fetchone()
	
	def _set_conf(self,channel,admin):
		self.bot.database.cursor.execute('DELETE FROM  "auto-role"')
		self.bot.database.cursor.execute('INSERT INTO  "auto-role" (channel, admin) VALUES (?,?)',(channel,admin))
		self.bot.database.c.commit()

	def _create_new_message(self,title,text,channel):
		self.bot.database.cursor.execute('INSERT INTO  "auto-role-messages" (title, text,channel) VALUES (?,?,?)',(title,text,channel))
		self.bot.database.c.commit()
		return self.bot.database.cursor.lastrowid

	def _get_message(self,id):
		self.bot.database.cursor.execute('SELECT id, messageid, title, text, channel FROM "auto-role-messages" WHERE id = ?',(id,))
		return self.bot.database.cursor.fetchone()

	def _delete_message(self,id):
		self.bot.database.cursor.execute('DELETE FROM "auto-role-messages" WHERE id = ?',(id,))
		self.bot.database.c.commit()
		self.bot.database.cursor.execute('DELETE FROM "auto-role-options" WHERE messageid = ?',(id,))
		self.bot.database.c.commit()

	def _get_message_options_id(self,id):
		self.bot.database.cursor.execute('SELECT icon,icontext,action,role FROM "auto-role-options" WHERE messageid = ?',(id,))
		return self.bot.database.cursor.fetchall()

	def _add_option(self,id,icon,text,action,role):
		self.bot.database.cursor.execute('INSERT INTO "auto-role-options" (messageid,icon,icontext,action,role) VALUES (?,?,?,?,?)',(id,icon,text,action,role))
		self.bot.database.c.commit()

	def _remove_option(self,id, icon):
		self.bot.database.cursor.execute('DELETE FROM "auto-role-options" WHERE messageid = ? and icon = ?',(id,icon))
		self.bot.database.c.commit()

	def _set_unpublished(self,id):
		self.bot.database.cursor.execute('UPDATE "auto-role-messages" SET messageid = "" WHERE id = ?',(id,))
		self.bot.database.c.commit()

	async def __autorole(self,ctx):
		conf = self._get_conf() 
		msg = "__**Auto Role configuration**__\n"
		await ctx.send(msg + "\n**Default channel**: " + conf[0] + "\n**Admin channel:**: " + conf[1])

	async def __autorole_conf(self,ctx,channel,admin):
		self._set_conf(channel,admin)
		await self.__autorole(ctx)
	
	async def __autorole_messages(self,ctx):
		messages = self._get_messages()
		msg = "__**Auto Role messages**__\n"
		for msg in messages:
			await ctx.send("\n**id**: " + str(msg[0]) + "\n**title**: " + msg[2])
	
	async def __autorole_create_new_message(self,ctx,title,text,channel):
		id = self._create_new_message(title,text,channel)
		msg = self._get_message(id)
		await ctx.send("\n**id**: " + str(msg[0]) + "\n**title**: " + msg[2] + "\n**text**: ```" + msg[3] + "```\n**channel**: " + str(msg[4]) + "\n**publishedmsg**: " + str(msg[1]) + "\n---------------------- ")

	async def __delete_message(self,ctx,id):
		msg = self._get_message(id)
		if msg[1]:
			if str(msg[4]) == "None" :
				channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
			else:
				channel = discord.utils.get(self.bot.guilds[0].channels, name=msg[4])
			message = await channel.fetch_message(msg[1])
			await message.delete()
		self._delete_message(id)
		await self.__autorole_messages(ctx)

	async def __show_messageid(self,ctx,id):
		msg = self._get_message(id)
		await ctx.send("\n**id**: " + str(msg[0]) + "\n**title**: " + msg[2] + "\n**text**: ```" + msg[3] + "```\n**channel**: " + str(msg[4]) + "\n**publishedmsg**: " + str(msg[1]) + "\n---------------------- ")

	async def __show_message_options(self,ctx,id):
		options = self._get_message_options_id(id)
		if len(options) >= 1:
			for msg in options:
				await ctx.send("\n**icon**: " + msg[0] + "\n**text**: " + msg[1] + "\n**action**: " + msg[2] + "\n**role**: " + str(msg[3]) + "\n--------")
		else:
			await ctx.send("\nNo options for this message")
			
	async def __autorole_add_option(self,ctx,id,icon,text,action,role):
		print(icon)
		self._add_option(id,icon,text,action,role)
		await self.__show_message_options(ctx,id)

	async def __autorole_remove_option(self,ctx,id,icon):
		self._remove_option(id,icon)
		await self.__show_message_options(ctx,id)

	async def __test_id(self,ctx,id):
		await self.autorole_publish(ctx,True,id)

	async def __publish_id(self,ctx,id):
		await self.autorole_publish(ctx,False,id)
	
	async def __publish_all(self,ctx):
		messages = self._get_messages()
		for msg in messages:
			await self.__publish_id(ctx,msg[0])
	
	async def __unpublish_id(self,ctx,id):
		message = self._get_message(id)
		channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
		if message[4] != "":
			channel = discord.utils.get(self.bot.guilds[0].channels, name=message[4])
		dmesg = await channel.fetch_message(message[1])
		await dmesg.delete()
		self._set_unpublished(id)

	@commands.command()
	@commands.has_role("botadmin")
	async def autorole(self, ctx, *args):
		"""
			Allows to create a question system and perform actions when user reacts to selected icons

			!autorole
				Shows the current configuration
			
			!autorole conf <default_channel> <admin_channel>
				Configures the <default_channel> where questions are posted, and defines the <admin_channel> qhere
				the admins get a notification if the autorole should be manual added
			
			!autorole messages
				Show the current stored messages
			
			!autorole messages <id>
				Show the details of the message <id>
			
			!autorole messages + <title> <text> <channel>
				Creates a new messages unpublished with <title> and <text> to be published in <channel>
			
			!autorole messages - <id>
				Removes de selected message

			!autorole options <id>
				Show the options for message <id>
			
			!autorole options + <message_id> <icon> <text> <action> <role>
				Creates a new option for <message_id> with the <icon> <text> and <action> over the <role>
				action: arole --> Automatic role assignment
						prole --> Message posted to admin. Manual assignment

			!autorole options - <message_id> <icon>
				Removes the option <icom> from <message_id>
			
			!autorole test <id>
				Creates a preview for message <id> in the current channel
			
			!autorole publish <id>
				Publish the message <id>
			
			!autorole publish all
				Publish all pending messages

			!autorole unpublish <id>
				Removes the message but keep it in the database

		"""
		if len(args) == 0:
			await self.__autorole(ctx)
			return
		elif len(args) == 1:
			if args[0] == 'messages':
				await self.__autorole_messages(ctx)
				return
		elif len(args) == 2:
			if args[0] == 'messages':
				await self.__show_messageid(ctx,args[1])
				return
			elif args[0] == 'options':
				await self.__show_message_options(ctx,args[1])
				return
			elif args[0] == 'test':
				await self.__test_id(ctx,args[1])
				return
			elif args[0] == 'publish' and args[1] == 'all':
				await self.__publish_all(ctx)
				return
			elif args[0] == 'publish':
				await self.__publish_id(ctx,args[1])
				return
			elif args[0] == 'unpublish':
				await self.__unpublish_id(ctx,args[1])
				return
		elif len(args) == 3:
			if args[0] == 'conf':
				await self.__autorole_conf(ctx,args[1],args[2])
				return
			elif args[0] == 'messages' and args[1] == "-":
				await self.__delete_message(ctx,args[2])
				return
		elif len(args) == 4:
			if args[0] == 'options' and args[1] == '-':
				await self.__autorole_remove_option(ctx,args[2],args[3])
				return
		elif len(args) == 5:
			if args[0] == 'messages' and args[1] == '+':
				await self.__autorole_create_new_message(ctx,args[2],args[3],args[4])
				return
		elif len(args) == 7:
			if args[0] == 'options' and args[1] == '+':
				await self.__autorole_add_option(ctx,args[2],args[3],args[4],args[5],args[6])
				return
		await ctx.send("🔔 Unrecognized command 🔔")


	async def autorole_publish(self, ctx,test,id):
		def get_emoji_icon(icon):
			if icon[0] == ":": # Integrated emoji
				return icon
			else:
				emoji = discord.utils.get(self.bot.guilds[0].emojis,name=icon)
				return str(emoji)

		def get_reaction_icon(icon):
			#if icon[0] == ":":
			#	return emoji.emojize(icon,use_aliases=True)
			#else:
			#	return discord.utils.get(self.bot.guilds[0].emojis,name=icon)
			return icon
				
		def create_options(options):
			# option[0] icon
			# option[1] icontext
			# option[2] action
			# option[3] role
			optiontext = '\n\n'
			for option in options:
				icon = option[0]
				#optiontext += '\n    ' + get_emoji_icon(icon) + " " + option[1]
				optiontext += '\n    ' + icon + " " + option[1]
			optiontext +='\n'
			return optiontext

		async def create_message(self, message, test,ctx):
			# message[0] id
			# message[1] messageid
			# message[2] title
			# message[3] text
			# message[4] channel
			embedmessage = discord.Embed()
			embedmessage.title = message[2]
			embedmessage.colour = discord.Colour.dark_gold()
			options = self._get_message_options_by_id((str(message[0]),))
			embedmessage.description = message[3] + create_options(options)
			channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
			if message[1] == ""  or str(message[1]) == 'None' or test:
				if message[4] != "":
					channel = discord.utils.get(self.bot.guilds[0].channels, name=message[4])
				if test:
					channel = ctx.channel
				msg = await channel.send(embed=embedmessage) 
				if not test:
					self._save_message_id((msg.id,int(message[0])))
				# option[0] is icon
				# option[2] is action
				for option in options:
					print(option[0])
					await msg.add_reaction(option[0])
				logging.info(f'auto-role message created {message[2]}')
			else:
				dmesg = await channel.fetch_message(message[1])
				await dmesg.edit(embed=embedmessage)
				for option in options:
					await dmesg.add_reaction(get_reaction_icon(option[0]))
    	# --------------------
		
		try:
			if (test):
				message = self._get_message(id)
				await create_message(self,message,test,ctx)
			else:
				message = self._get_message(id)
				await create_message(self, message, test, ctx)
		except Exception as e:
			logging.error(f"Error {e}: {sys.exc_info()[0]}")
			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			print(exc_type, fname, exc_tb.tb_lineno)

		
	@commands.Cog.listener()
	async def on_raw_reaction_add(self, payload):	
		if payload.user_id == self.bot.user.id:
			return # Bot itself reactions are ignored
		if self._check_message_belongs_to_autorole(str(payload.message_id)):
			if self.check_reaction_in_options(payload):
				await self.action(payload)
			else:
				logging.warning("User reacting to not allowed auto-role icon")
				await self.remove_reaction(payload)

	@commands.Cog.listener()
	async def on_raw_reaction_remove(self, payload):
		if payload.user_id == self.bot.user.id:
			return # Bot itself reactions are ignored
		if self._check_message_belongs_to_autorole(str(payload.message_id)):
			if self.check_reaction_in_options(payload):
				await self.action_remove(payload)

	async def action_remove(self, payload):
		options = self._get_message_options((str(payload.message_id),))
		for option in options:
			if self.convert_icon(option[0]) == str(payload.emoji):
				# optionr[2] is the action
				if option[2] == "arole":
					await self.action_arole_remove(option,payload)
				elif option[2] == "prole":
					await self.action_prole_remove(option,payload)
				else:
					print("NO action for role" + payload)

	async def action_arole_remove(self, option, payload):
		# role is option[3]
		role = discord.utils.get(self.bot.guilds[0].roles, name=option[3])
		if (role == None):
			logging.error("ERROR AutoRole don't exists: " + option[3])
			return
		member = self.bot.guilds[0].get_member(payload.user_id)
		await member.remove_roles(discord.Object(role.id), reason="AutorRole module")
		logging.info(f"Removed user: {member.name} role: {option[3]}")


	async def action_prole_remove(self, trigger, payload):
		await self.action_arole_remove(trigger,payload)
 
	async def action(self, payload):
		options = self._get_message_options((str(payload.message_id),))
		for option in options:
			# option[0] is icon
			# option[2] is action
			if option[0] == str(payload.emoji):
				if option[2] == "arole":
					await self.action_arole(option, payload)
				elif option[2] == "prole":
					await self.action_prole(option, payload)
				else:
					logging.info("NO action for role" + payload)

	async def action_arole(self, option, payload):
		# option[3] is role name
		role = discord.utils.get(self.bot.guilds[0].roles, name=option[3])
		if (role == None):
			logging.error("ERROR AutoRole don't exists: " + option[3])
			return
		member = self.bot.guilds[0].get_member(payload.user_id)
		await member.add_roles(discord.Object(role.id), reason="AutorRole module")
		logging.info(f"Arole user: {member.name} role: {option[3]}")

	async def action_prole(self, option, payload):
		member = self.bot.guilds[0].get_member(payload.user_id)
		channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["admin"])
		embedmessage = discord.Embed()
		embedmessage.title = "🔔 Autorole role request 🔔"
		embedmessage.colour = discord.Colour.dark_gold()
		# option[3] is role name
		embedmessage.description ="User: **" + member.name + "**\n\nRole: **" + option[3] + "**\n"
		await channel.send(embed=embedmessage)
		logging.info(f"Prole user: {member.name} role: {option[3]}")

	def convert_icon(self, icon):
		if icon[0] == ":":
			return emoji.emojize(icon,use_aliases=True)
		else:
			return icon

	def check_reaction_in_options(self, payload):
		options = self._get_message_options((str(payload.message_id),))
		for option in options:
			#if self.convert_icon(option[0]) == payload.emoji.name:
			if option[0] == str(payload.emoji):
				return True
		return False
	
	async def remove_reaction(self, payload):
		channel = self.bot.guilds[0].get_channel(payload.channel_id)
		message = await channel.fetch_message(payload.message_id)
		await message.remove_reaction(payload.emoji, discord.Object(payload.user_id))
		logging.info(f"Removed not allowed reaction {payload.emoji.name}")

# initialize plugin
def setup(bot):
	bot.add_cog(AutoRole(bot))
def teardown(bot):
    bot.remove_cog('auto-role')
