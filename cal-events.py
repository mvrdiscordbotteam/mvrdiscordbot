import datetime
from datetime import timedelta
import discord
from discord.ext import commands
from discord.ext import tasks
import logging
import icalendar
import recurring_ical_events
import urllib.request
import pytz
from pytz import timezone
import io
import aiohttp
import re
import sys

class CalEvents(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
        self.update_calendar.change_interval(seconds=self.config["interval"])
        self.update_calendar.start()
    
    def cog_unload(self):
        self.update_calendar.cancel()

    def _load_config(self):
        self.bot.database.cursor.execute('SELECT url, interval, channel, nrole, rrole, remainder1, remainder2 FROM "cal-events"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["url"] = tmp[0]
        self.config["interval"] = tmp[1]
        self.config["channel"] = tmp[2]
        self.config["nrole"] = tmp[3]
        self.config["rrole"] = tmp[4]
        self.config["remainder1"] = tmp[5]
        self.config["remainder2"] = tmp[6]

    def _event_in_db(self, uid, date):
        self.bot.database.cursor.execute('SELECT uid FROM "cal-events-list" WHERE uid = ? AND date =  ?',(uid, date))
        tmp = self.bot.database.cursor.fetchall()
        if len(tmp) > 0:
            return True
        else:
            return False

    def _check_message_belongs_to_cal_events(self, messageid):
        self.bot.database.cursor.execute('SELECT messageid FROM "cal-events-list" WHERE messageid = ?',(messageid,))
        tmp = self.bot.database.cursor.fetchall()
        if len(tmp) > 0:
            return True
        else:
            return False
	
    def _save_new_event(self, uid, title, date, description, image):
        self.bot.database.cursor.execute('INSERT INTO "cal-events-list" (uid, title, date, description,image) VALUES (?,?,?, ?, ?)', (uid,title,date,description,image))
        self.bot.database.c.commit()
    
    def _get_due_events(self,date):
        self.bot.database.cursor.execute('SELECT uid,messageid,date FROM "cal-events-list" WHERE date <  ?',(date,))
        return self.bot.database.cursor.fetchall()
    
    def _get_events_published_to_notify(self, date, state):
        self.bot.database.cursor.execute('SELECT uid,messageid,title,date FROM "cal-events-list" WHERE date <  ? AND remainder = ? AND published = 1 ',(date,state))
        return self.bot.database.cursor.fetchall()

    def _delete_event(self, message):
        self.bot.database.cursor.execute('DELETE FROM "cal-events-list" WHERE uid = ? AND date = ?', message)
        self.bot.database.c.commit()

    def _get_no_published_messages(self):
        self.bot.database.cursor.execute('SELECT uid,title,description,date,image FROM "cal-events-list" WHERE messageid is null order by date')
        return self.bot.database.cursor.fetchall()
    
    def _set_published(self, msgid, uid, date):
        params = (msgid,uid,date)
        self.bot.database.cursor.execute('UPDATE "cal-events-list" SET messageid = ?, published = 1 WHERE uid = ? AND date = ?', params)
        self.bot.database.c.commit()

    def _set_reminder(self, messageid, newstate):
        self.bot.database.cursor.execute('UPDATE "cal-events-list" SET remainder = ? WHERE messageid= ?', (newstate, messageid))
        self.bot.database.c.commit()

    def _get_event_data_from_message_id(self, messageid):
        self.bot.database.cursor.execute('SELECT title, date FROM "cal-events-list" WHERE messageid = ?',(messageid,))
        tmp = self.bot.database.cursor.fetchone()
        return tmp
    
    def _get_all_events(self):
        self.bot.database.cursor.execute('SELECT messageid FROM "cal-events-list"')
        return self.bot.database.cursor.fetchall()

    def _get_conf(self):
        self.bot.database.cursor.execute('SELECT url, interval, channel, nrole, rrole, remainder1, remainder2 FROM "cal-events"')
        return self.bot.database.cursor.fetchone()

    def _set_conf(self, url,channel,rolen,rolera,interval,ft,st):
        self.bot.database.cursor.execute('DELETE FROM  "cal-events"')
        self.bot.database.cursor.execute('INSERT INTO  "cal-events" (url,channel,nrole,rrole,interval,remainder1,remainder2) VALUES (?,?,?,?,?,?,?)',(url,channel,rolen,rolera,interval,ft,st))
        self.bot.database.c.commit()
    
    def _clean(self):
        self.bot.database.cursor.execute('DELETE FROM  "cal-events-list"')
        self.bot.database.c.commit()

    async def __calendar(self,ctx):
        conf = self._get_conf() 
        msg = "__**Calendar configuration**__\n"
        msg += "\n**URL**: " + conf[0] 
        msg += "\n**Interval**: " + str(conf[1]) + " seconds"
        msg += "\n**Channel**: " + conf[2]
        msg += "\n**New Event Role name**: " + conf[3]
        msg += "\n**Event Reminder Role name**: " + conf[4]
        msg += "\n**Time for first reminder**: " + str(conf[5]) + " minutes"
        msg += "\n**Time for second reminder**: " + str(conf[6]) + " minutes"
        await ctx.send(msg)
    
    async def __calendar_clean(self,ctx):
        events = self._get_all_events()
        channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
        for event in events:
            message = await channel.fetch_message(event[0])
            await message.delete()
        self._clean()
        await ctx.send("\n🔔 All event removed 🔔\n")

    async def __calendar_conf(self,ctx,url,interval,channel,rolen,rolera,ft,st):
        self._set_conf(url,channel,rolen,rolera,interval,ft,st)
        await self.__calendar(ctx)

    @commands.command()
    @commands.has_role("botadmin")
    async def calendar(self, ctx, *args):
        """
            Allows to sync Google Calendar events in a specific channel

            !calendar
                Shows the current configuration
            
            !calendar conf <url > <interval> <channel> <role_new_event_alert> <role_reminder_alert> <first_remainder_timer> <second_remainder_timer>
                Sets the <channel> to post the new events registered in the Google Calendar <url>. The calendar is checked each <interval> seconds.
                Users with the <role_new_event_alert> are warned. Users subscribed to an event, and with the role <role_reminder_alert>
                are warned by PM at <first_reminder_timer> seconds, and at <second_reminder_timer> seconds

            !calendar clean
                Removes old events and clears database

        """
        if len(args) == 0:
            await self.__calendar(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'clean':
                await self.__calendar_clean(ctx)
                return
        elif len(args) == 8:
            if args[0] == 'conf':
                await self.__calendar_conf(ctx,args[1],args[2],args[3],args[4],args[5],args[6],args[7])
                return
        await ctx.send("🔔 Unrecognized command 🔔")


    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):	
        if payload.user_id == self.bot.user.id:
            return # Bot itself reactions are ignored
        if self._check_message_belongs_to_cal_events(str(payload.message_id)):
            if payload.emoji.name == "✅":
                await self.warn_new_user_to_event(payload)
            else:
                await self.remove_reaction(payload)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        if payload.user_id == self.bot.user.id:
            return # Bot itself reactions are ignored
        if self._check_message_belongs_to_cal_events(str(payload.message_id)):
             if payload.emoji.name== "✅":
                await self.warn_del_user_to_event(payload)
        
    @tasks.loop(seconds=1.0)
    async def update_calendar(self):
        logging.info("Starting update_calendar task")
        try:
            ical_string = urllib.request.urlopen(self.config["url"]).read()
            calendar = icalendar.cal.Calendar.from_ical(ical_string)
            amsterdam = timezone('Europe/Amsterdam')
            washinton = timezone('US/Eastern')
            start_date = datetime.date.today()
            start_date_time = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
            events = recurring_ical_events.of(calendar).between(start_date, start_date + timedelta(days=7))
            for event in events:
                if  not self._event_in_db(event.get("UID"),event.get("DTSTART").dt.astimezone(pytz.utc)):
                    self._save_new_event(event.get("UID"), event.get("SUMMARY"), event.get("DTSTART").dt.astimezone(pytz.utc), event.get("DESCRIPTION"),event.get("LOCATION") )
                    logging.info(f'Saved new event {event.get("SUMMARY")}')
            for component in calendar.walk():
                if component.name == "VEVENT":
                    if component.get("DTSTART").dt >= start_date_time:
                        if ( not self._event_in_db(component.get("UID"),component.get("DTSTART").dt.astimezone(pytz.utc))):
                            self._save_new_event(component.get("UID"), component.get("SUMMARY"), component.get("DTSTART").dt.astimezone(pytz.utc), component.get("DESCRIPTION"),event.get("LOCATION") )
                            logging.info(f'Saved new event {event.get("SUMMARY")}')
        
            await self.update_events_in_discord()
            await self.notify_reminders()
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")


    @update_calendar.before_loop
    async def before_update_calendar(self):
        logging.info("Waiting update_calendar to get ready")
        await self.bot.wait_until_ready()

    async def notify_reminders(self):
        events = self._get_events_published_to_notify(datetime.datetime.utcnow() + timedelta(seconds=(60*self.config["remainder1"])),0)
        for event in events:
            await self.remainder(event,1,"⏰ **__Event Remainder 24h__** ⏰\nRemember, tomorrow you will join the event:")
        events = self._get_events_published_to_notify(datetime.datetime.utcnow() + timedelta(seconds=(60*self.config["remainder2"])),1)
        for event in events:
            await self.remainder(event,2,"⏰ **__Event Remainder 1h__** ⏰\nRemember, in less than an hour the event:")

    async def remainder(self, eventitem, newstate, text):
        channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
        message = await channel.fetch_message(eventitem[1])
        msg = text
        msg += "\n** **\n"
        event = self._get_event_data_from_message_id(eventitem[1])
        timeutc = datetime.datetime.strptime(event[1],'%Y-%m-%d %H:%M:%S%z')
        timeEST = timeutc.astimezone(timezone('US/Eastern'))
        timeCET = timeutc.astimezone(timezone('Europe/Berlin'))
        times = "```ini\n" + timeEST.strftime("[EST]  %a, %d %b, at %H:%M")
        times += "\n" + timeutc.strftime('[UTC]  %a, %d %b, at %H:%M')
        times += "\n" + timeCET.strftime('[CET]  %a, %d %b, at %H:%M') + "```"
        msg += "**" + event[0] + "**"
        msg += times
        for reaction in message.reactions:
            if reaction.emoji == "✅":
                msg += "\nThere are **" + str(reaction.count - 1) + "** 👫 that will join the event"
                users = await reaction.users().flatten()
                for user in users:
                    if (not user.bot):
                        for role in user.roles:
                            if role.name == self.config["rrole"]:
                                await user.create_dm()
                                await user.dm_channel.send(msg)
                                break
                self._set_reminder(eventitem[1], newstate) 

    async def warn_new_user_to_event(self, payload):
        channel = self.bot.guilds[0].get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        member = discord.utils.get(message.guild.members, id=payload.user_id)
        msg = "📢 **__New user to event__** 📢\n"
        msg += "The user 🎊 **" + str(member.name) + "** 🎊 joined the event:"
        msg += "\n** **\n"
        event = self._get_event_data_from_message_id(payload.message_id)
        timeutc = datetime.datetime.strptime(event[1],'%Y-%m-%d %H:%M:%S%z')
        timeEST = timeutc.astimezone(timezone('US/Eastern'))
        timeCET = timeutc.astimezone(timezone('Europe/Berlin'))
        times = "```ini\n" + timeEST.strftime("[EST]  %a, %d %b, at %H:%M")
        times += "\n" + timeutc.strftime('[UTC]  %a, %d %b, at %H:%M')
        times += "\n" + timeCET.strftime('[CET]  %a, %d %b, at %H:%M') + "```"
        msg += "**" + event[0] + "**"
        msg += times
        logging.info(f"New user {member.name} in event {event[0]}")
        for reaction in message.reactions:
            if reaction.emoji == "✅":
                msg += "\nThere are **" + str(reaction.count -1) + "** 👫 that will join the event"
                users = await reaction.users().flatten()
                for user in users:
                    if (not user.bot) and (user.id != payload.user_id):
                        for role in user.roles:
                            if role.name == self.config["nrole"]:
                                await user.create_dm()
                                await user.dm_channel.send(msg)
                                break

    async def warn_del_user_to_event(self, payload):
        channel = self.bot.guilds[0].get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        member = discord.utils.get(message.guild.members, id=payload.user_id)
        msg = "📢 **__User left the event__** 📢\n"
        msg += "The user 😭 **" + str(member.name) + "** 😭 left the event:"
        msg += "\n** **\n"
        event = self._get_event_data_from_message_id(payload.message_id)
        timeutc = datetime.datetime.strptime(event[1],'%Y-%m-%d %H:%M:%S%z')
        timeEST = timeutc.astimezone(timezone('US/Eastern'))
        timeCET = timeutc.astimezone(timezone('Europe/Berlin'))
        times = "```ini\n" + timeEST.strftime("[EST]  %a, %d %b, at %H:%M")
        times += "\n" + timeutc.strftime('[UTC]  %a, %d %b, at %H:%M')
        times += "\n" + timeCET.strftime('[CET]  %a, %d %b, at %H:%M') + "```"
        msg += "**" + event[0] + "**"
        msg += times
        logging.info(f"Removed user {member.name} in event {event[0]}")
        for reaction in message.reactions:
            if reaction.emoji == "✅":
                msg += "\nThere are **" + str(reaction.count -1) + "** 👫 that will join the event"
                users = await reaction.users().flatten()
                for user in users:
                    if (not user.bot) and (user.id != payload.user_id):
                        for role in user.roles:
                            if role.name == self.config["nrole"]:
                                await user.create_dm()
                                await user.dm_channel.send(msg)
                                break

                        
    async def remove_reaction(self, payload):
        channel = self.bot.guilds[0].get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        await message.remove_reaction(payload.emoji, discord.Object(payload.user_id))
        logging.info("Reaction removed from calendar: No authorized icon")

    async def update_events_in_discord(self):
        await self.remove_old_events()
        await self.publish_new_events()

    async def remove_old_events(self):
        messages = self._get_due_events(datetime.datetime.utcnow() - timedelta(seconds=3600))
        for message in messages:
            try:
                # Delete from discord
                channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
                if channel != None and message[1] != None:
                    msg = await channel.fetch_message(message[1])
                    await msg.delete()
            except Exception as e:
                logging.error(f"Error {e}: {sys.exc_info()[0]}")
            self._delete_event((message[0],message[2]))

    async def publish_new_events(self):
        messages = self._get_no_published_messages()
        channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
        for message in messages:
            timeutc = datetime.datetime.strptime(message[3],'%Y-%m-%d %H:%M:%S%z')
            timeEST = timeutc.astimezone(timezone('US/Eastern'))
            timeCET = timeutc.astimezone(timezone('Europe/Berlin'))
            times = "**Time**\n"
            times += "```ini\n" + timeEST.strftime("[EST]  %a, %d %b, at %H:%M")
            times += "\n" + timeutc.strftime('[UTC]  %a, %d %b, at %H:%M')
            times += "\n" + timeCET.strftime('[CET]  %a, %d %b, at %H:%M')
            msg = "📢 🔻🔻🔻🔻🔻🔻🔻🔻\n"
            msg += "**" + message[1] + "**\n" + message[2] + "\n\n" + times + "```\n "
            msg+="(*Remember to react to the ✅ icon if you want to join the event*)\n"
            # We check if the event has an image/banner message[4]
            if  message[4] != "":
                my_url =  message[4]
                async with aiohttp.ClientSession() as session:
                    async with session.get(my_url) as resp:
                        if resp.status != 200:
                            msg = await channel.send(msg)
                        else:
                            data = io.BytesIO(await resp.read())
                            msg = await channel.send(msg,file=discord.File(data, 'cool_image.png'))
            else:
                msg = await channel.send(msg)
            self._set_published(msg.id,message[0], message[3])
            await msg.add_reaction("✅")
    


# initialize plugin
def setup(bot):
	bot.add_cog(CalEvents(bot))
def teardown(bot):
    bot.remove_cog('cal-events')