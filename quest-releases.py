from discord.ext import tasks
import discord
import datetime
from discord.ext import commands
import logging
import requests
import sys

class QuestReleases(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
        self.check_new_quest_releases.change_interval(seconds=self.config["interval"])
        self.check_new_quest_releases.start()
    
    def cog_unload(self):
        self.check_new_quest_releases.cancel()
    
    def _load_config(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "quest-releases"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["channel"] = tmp[0]
        self.config["role"] = tmp[1]
        self.config["interval"] = tmp[2]

    def _get_quest_games_in_database(self):
        self.bot.database.cursor.execute('SELECT id FROM "quest-releases-games"')
        tmp = self.bot.database.cursor.fetchall()
        tmp = [item for t in tmp for item in t] 
        return tmp

    def _save_game_in_database(self, game):
         self.bot.database.cursor.execute('INSERT INTO "quest-releases-games" (id, display_name, date) VALUES (?,?,?)', game)
         self.bot.database.c.commit()

    def _get_conf(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "quest-releases"')
        return self.bot.database.cursor.fetchone()

    def _set_conf(self,channel, role, interval):
        self.bot.database.cursor.execute('DELETE FROM  "quest-releases"')
        self.bot.database.cursor.execute('INSERT INTO  "quest-releases" (channel, role, interval) VALUES (?,?,?)',(channel,role,interval))
        self.bot.database.c.commit()
    
    def _clean(self):
        self.bot.database.cursor.execute('DELETE FROM  "quest-releases-games"')
        self.bot.database.c.commit()

    async def __questreleases(self,ctx):
        conf = self._get_conf() 
        msg = "__**Quest Releases configuration**__\n"
        await ctx.send(msg + "\n**Channel**: " + conf[0] + "\n**Role for alerts:**: " + conf[1] + "\n**Retrieval interval**: " + str(conf[2]) + " seconds")
    
    async def __questreleases_clean(self,ctx):
        self._clean()
        await ctx.send("\n🔔 All games removed 🔔\n")

    async def __questreleases_conf(self,ctx,channel,role,interval):
        self._set_conf(channel,role,interval)
        await self.__questreleases(ctx)

    @commands.command()
    @commands.has_role("botadmin")
    async def questreleases(self, ctx, *args):
        """
            Allows to post when new quest games are available

            !questreleases
                Shows the current configuration
            
            !questreleases conf <channel> <role> <interval>
                Sets the <channel> to post the new games, with mention <role> for alerts, and the frecuency <interval> in seconds new games are checked

            !questreleases clean
                Removes quest game database

        """
        if len(args) == 0:
            await self.__questreleases(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'clean':
                await self.__questreleases_clean(ctx)
                return
        elif len(args) == 4:
            if args[0] == 'conf':
                await self.__questreleases_conf(ctx,args[1],args[2],args[3])
                return
        await ctx.send("🔔 Unrecognized command 🔔")



    @tasks.loop(seconds=1.0)
    async def check_new_quest_releases(self):
        logging.info("Starting check_new_quest_releases task")
        try:
            payload = {
                "access_token": 'OC|1317831034909742|',
                "variables": {
                    "sectionId": "226705144927006",
                    "sortOrder": None,
                    "sectionItemCount": 60,
                    "sectionCursor": None,
                    "hmdType": "MONTEREY"
                },
                        "doc_id": "1934814353250664"
            }
            role = discord.utils.get(self.bot.guilds[0].roles, name=self.config["role"])
            r = requests.post('https://graph.oculus.com/graphql', json=payload)
            channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
            
            db = self._get_quest_games_in_database()

            for game in r.json()["data"]["node"]["all_items"]["edges"]:
                if not game["node"]["id"] in db:
                    embedmessage = discord.Embed()
                    embedmessage.title = game["node"]["display_name"]
                    embedmessage.set_author(name="New Quest game/experience")
                    embedmessage.set_image(url=game["node"]["cover_landscape_image"]["uri"])
                    embedmessage.colour = discord.Colour.dark_purple()
                    embedmessage.url = "https://www.oculus.com/experiences/quest/" + \
                        game["node"]["id"]
                    embedmessage.description = game["node"]["current_offer"]["price"]["formatted"]
                    datetext = datetime.datetime.fromtimestamp(int(game["node"]["release_date"])).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    embedmessage.set_footer(text=datetext)
                    if role == None:
                        await channel.send(embed=embedmessage)
                    else:
                        await channel.send("\n" + role.mention + "\n", embed=embedmessage)
                    self._save_game_in_database((game["node"]["id"],game["node"]["display_name"],datetext))
                    logging.info(f'New Quest release: {game["node"]["display_name"]}')
        except Exception as e:
            logging.error(f"Error {e}: {sys.exc_info()[0]}")


     # Wait until bot is ready to retrieve quest games
    @check_new_quest_releases.before_loop
    async def before_check_new_quest_releases(self):
        logging.info("Waiting check_new_quest_releases to get ready")
        await self.bot.wait_until_ready() 

# initialize plugin
def setup(bot):
	bot.add_cog(QuestReleases(bot))

# Remove extension
def teardown(bot):
    bot.remove_cog('quest-releases')
