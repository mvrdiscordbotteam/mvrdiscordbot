from discord.ext import commands

class Kernel(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
    
    def _get_feautures(self):
        self.bot.database.cursor.execute('SELECT name, active FROM "features"')
        return self.bot.database.cursor.fetchall()
    
    def _feautures_add(self,name):
        self.bot.database.cursor.execute('UPDATE "features" SET active = 1 WHERE name = ?',(name,))
        self.bot.database.c.commit()

    def _feautures_remove(self,name):
        self.bot.database.cursor.execute('UPDATE "features" SET active = 0 WHERE name = ?',(name,))
        self.bot.database.c.commit()

    async def __feautures(self,ctx):
        msg = "__**Features status**__\n"
        feautures = self._get_feautures()
        for feauture in feautures:
            if feauture[1] == 1:
                msg += "\n ✅"
            else:
                msg += "\n 🚫"
            msg +=  feauture[0] 
        await ctx.send(msg)
    
    async def __feautures_add(self,ctx,name):
        try:
            self._feautures_add(name)
            await self.__feautures(ctx)
        except Exception as e:
            await ctx.send(f"Error {e}")

    async def __feautures_remove(self,ctx,name):
        try:
            self._feautures_remove(name)
            await self.__feautures(ctx)
        except Exception as e:
            await ctx.send(f"Error {e}")

    @commands.command()
    async def reload(self, ctx):
        """ Reloads configuration
            It's needed when you enable/disable a feature
        """
        self.bot.reload_features()
        await ctx.send("✅ Features reloaded")

    @commands.command()
    @commands.has_role("botadmin")
    async def features(self, ctx, *args):
        """
            Allows to enable/disable main features in bot

            !features
                Shows the features enabled/disabled
            
            !features + <feature_name>
                Enable feature
            
            !features - <feature_name>
                Disable feature

            Remember to call 'reload' command for effectively confirm the changes
        """
        if len(args) == 0:
            await self.__feautures(ctx)
            return
        elif len(args) == 2:
            if args[0] == '+':
                await self.__feautures_add(ctx,args[1])
                return
            elif args[0] == '-':
                await self.__feautures_remove(ctx,args[1])
                return
        await ctx.send("🔔 Unrecognized command 🔔")

# initialize plugin
def setup(bot):
	bot.add_cog(Kernel(bot))
