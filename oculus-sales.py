import datetime
import discord
from discord.ext import commands
from discord.ext import tasks
import requests
from datetime import datetime
import sys
import logging

class OculusSales(commands.Cog):
    def __init__(self,bot):
        self.bot = bot
        self.config = {}
        self._load_config()
        self.check_oculus_sales.change_interval(seconds=self.config["interval"])
        self.check_oculus_sales.start()
    
    def cog_unload(self):
        self.check_oculus_sales.cancel()

    def _load_config(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "oculus-sales"')
        tmp = self.bot.database.cursor.fetchone()
        self.config["channel"] = tmp[0]
        self.config["role"] = tmp[1]
        self.config["interval"] = tmp[2]

    def _get_database_offers(self):
        self.bot.database.cursor.execute('SELECT id FROM "oculus-sales-games"')
        tmp = self.bot.database.cursor.fetchall()
        tmp = [item for t in tmp for item in t] 
        return tmp

    def _set_end_sale(self,id):
        self.bot.database.cursor.execute('UPDATE "oculus-sales-games" SET updated = 0, published = 0,promo = null WHERE id = ?',(id,))
        self.bot.database.c.commit()

    def _save_new_game(self,id, display, image, expires, promo,published,onsale):
        self.bot.database.cursor.execute('INSERT INTO "oculus-sales-games" (id, display_name, imageURL, expires, published, updated, promo) VALUES (?,?,?,?,?,?,?)', (id,display,image,expires,published,onsale,promo))
        self.bot.database.c.commit()

    def _set_new_sale(self, id, promo, date, currentprice, price):
        self.bot.database.cursor.execute('UPDATE "oculus-sales-games" SET updated = 1, published = 0, promo = ?, expires = ?, currentprice = ?, price = ? WHERE id = ?',(promo,date,currentprice,price,id))
        self.bot.database.c.commit()
    
    def _set_different_sale(self, id, promo, date, currentprice, price):
        self.bot.database.cursor.execute('UPDATE "oculus-sales-games" SET updated = 1, published = 0, promo = ?, expires = ?, currentprice = ?, price = ?  WHERE id = ?',(promo,date,currentprice,price,id))
        self.bot.database.c.commit()
    
    def _get_db_promo(self,id):
        self.bot.database.cursor.execute('SELECT promo FROM "oculus-sales-games" WHERE id = ?',(id,))
        tmp = self.bot.database.cursor.fetchone()
        return tmp[0]

    def _is_diferent_promotion(self, id, promo):
        if promo == None:
            self.bot.database.cursor.execute('SELECT id FROM "oculus-sales-games" WHERE id = ? AND promo is NULL' ,(id,))
        else:
            self.bot.database.cursor.execute('SELECT id FROM "oculus-sales-games" WHERE id = ? AND promo = ? ' ,(id,promo))
        tmp = self.bot.database.cursor.fetchall()
        if len(tmp) > 0:
            return False
        else:
            return True

    def _set_updated_items_to_0(self):
        self.bot.database.cursor.execute('UPDATE "oculus-sales-games" SET updated = 0')
        self.bot.database.c.commit()

    def _save_new_sale(self, id, display, image, expires, promo, published, updated, currentprice, price):
        self.bot.database.cursor.execute('INSERT INTO "oculus-sales-games" (id, display_name, imageURL, expires, published, updated, promo, currentprice, price) VALUES (?,?,?,?,?,?,?,?,?)', (id,display,image,expires,published,updated,promo,currentprice,price))
        self.bot.database.c.commit()

    def _update_new_sale(self, id, display, image, expires, promo):
        self.bot.database.cursor.execute('UPDATE "oculus-sales-games" SET display_name = ?, imageURL = ?, expires = ?, published = 0, updated = 1, promo = ? WHERE id = ?', (display,image,expires,promo,id))
        self.bot.database.c.commit()

    def _get_current_no_already_published_sales(self):
        self.bot.database.cursor.execute('SELECT display_name, expires, imageURL, promo FROM "oculus-sales-games" WHERE published = 0 order by expires, display_name')
        return self.bot.database.cursor.fetchall()

    def _set_still_in_offer(self, id):
        self.bot.database.cursor.execute('UPDATE "oculus-sales-games" SET updated = 1 WHERE id = ?',(id,))
        self.bot.database.c.commit()

    def _get_expired_offers(self):
        self.bot.database.cursor.execute('SELECT display_name FROM "oculus-sales-games" WHERE updated = 0 order by display_name')
        return self.bot.database.cursor.fetchall()

    def _remove_expired_offers(self):
        self.bot.database.cursor.execute('DELETE FROM "oculus-sales-games" WHERE updated = 0')
        self.bot.database.c.commit()
    
    def _get_current_sales(self):
        self.bot.database.cursor.execute('SELECT display_name, expires, imageURL, promo,id,currentprice,price FROM "oculus-sales-games" WHERE published = 0 and updated = 1 order by expires, display_name')
        return self.bot.database.cursor.fetchall()

    def _get_ended_offers(self):
        self.bot.database.cursor.execute('SELECT display_name FROM "oculus-sales-games" WHERE updated = 0 and published = 0 order by display_name')
        return self.bot.database.cursor.fetchall()


    def _set_published(self):
        self.bot.database.cursor.execute('UPDATE "oculus-sales-games" SET published = 1')
        self.bot.database.c.commit()


    def _get_conf(self):
        self.bot.database.cursor.execute('SELECT channel, role, interval FROM "oculus-sales"')
        return self.bot.database.cursor.fetchone()

    def _set_conf(self,channel, role, interval):
        self.bot.database.cursor.execute('DELETE FROM  "oculus-sales"')
        self.bot.database.cursor.execute('INSERT INTO  "oculus-sales" (channel, role, interval) VALUES (?,?,?)',(channel,role,interval))
        self.bot.database.c.commit()
    
    def _clean(self):
        self.bot.database.cursor.execute('DELETE FROM  "oculus-sales-games"')
        self.bot.database.c.commit()

    async def __sales(self,ctx):
        conf = self._get_conf() 
        msg = "__**Oculus Sales configuration**__\n"
        await ctx.send(msg + "\n**Channel**: " + conf[0] + "\n**Role for alerts:**: " + conf[1] + "\n**Retrieval interval**: " + str(conf[2]) + " seconds")
    
    async def __sales_clean(self,ctx):
        self._clean()
        await ctx.send("\n🔔 All sales removed 🔔\n")

    async def __sales_conf(self,ctx,channel,role,interval):
        self._set_conf(channel,role,interval)
        await self.__sales(ctx)

    @commands.command()
    @commands.has_role("botadmin")
    async def sales(self, ctx, *args):
        """
            Allows to post when new game sales are available

            !sales
                Shows the current configuration
            
            !sales conf <channel> <role> <interval>
                Sets the <channel> to post the new games, with mention <role> for alerts, and the frecuency <interval> in seconds new sales are checked

            !sales clean
                Removes Sales game database

        """
        if len(args) == 0:
            await self.__sales(ctx)
            return
        elif len(args) == 1:
            if args[0] == 'clean':
                await self.__sales_clean(ctx)
                return
        elif len(args) == 4:
            if args[0] == 'conf':
                await self.__sales_conf(ctx,args[1],args[2],args[3])
                return
        await ctx.send("🔔 Unrecognized command 🔔")



    
    @tasks.loop(seconds=1.0)
    async def check_oculus_sales(self):
        logging.info("Starting check_oculus_sales task")
        try:
            payload = {
                "access_token": 'OC|1317831034909742|',
                "variables": {
                    #"sectionId": "1500175860035862",
                    "sectionId": "174868819587665",
                    "sortOrder": None,
                    #"sectionItemCount": 100,
                    "sectionItemCount": 10000,
                    "sectionCursor": None,
                    "hmdType": "PACIFIC"
                },
                #"doc_id": "1934814353250664"
                "doc_id": "2212117242196569"
            }
            role = discord.utils.get(self.bot.guilds[0].roles, name=self.config["role"])
            r = requests.post('https://graph.oculus.com/graphql', json=payload)
            channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
            # We reset our database to check if an offer is still valid, or is a new offer
            #self._set_updated_items_to_0()
            db = self._get_database_offers()
            for game in r.json()["data"]["node"]["all_items"]["edges"]:
                if not game["node"]["id"] in db:
                    if game["node"]["current_offer"]["promo_benefit"] == None:
                        self._save_new_sale(game["node"]["id"], game["node"]["display_name"], game["node"]["cover_landscape_image"]["uri"], game["node"]["current_offer"]["end_time"], game["node"]["current_offer"]["promo_benefit"],1,0,game["node"]["current_offer"]["price"]["formatted"],game["node"]["current_offer"]["price"]["formatted"])
                        logging.info(f'New gane: {game["node"]["display_name"]}')
                    else:
                        self._save_new_sale(game["node"]["id"], game["node"]["display_name"], game["node"]["cover_landscape_image"]["uri"], game["node"]["current_offer"]["end_time"], game["node"]["current_offer"]["promo_benefit"],0,1,game["node"]["current_offer"]["price"]["formatted"],game["node"]["current_offer"]["strikethrough_price"]["formatted"])
                        logging.info(f'New gane on Sale: {game["node"]["display_name"]}')
                else:
                    newsale = game["node"]["current_offer"]["promo_benefit"]
                    dbsale = self._get_db_promo( game["node"]["id"])
                    if str(newsale) != str(dbsale):
                        if str(newsale) == "None":
                            self._set_end_sale(game["node"]["id"])
                            logging.info(f'Ended sale: {game["node"]["display_name"]}')
                        elif str(dbsale) == "None":
                            self._set_new_sale(game["node"]["id"], newsale, game["node"]["current_offer"]["end_time"],game["node"]["current_offer"]["price"]["formatted"],game["node"]["current_offer"]["strikethrough_price"]["formatted"])
                            logging.info(f'New sale: {game["node"]["display_name"]}')
                        else:
                            self._set_different_sale(game["node"]["id"], newsale, game["node"]["current_offer"]["end_time"],game["node"]["current_offer"]["price"]["formatted"],game["node"]["current_offer"]["strikethrough_price"]["formatted"])
                            logging.info(f'Different sale: {game["node"]["display_name"]}')
                    #if self._is_diferent_promotion(game["node"]["id"], game["node"]["current_offer"]["promo_benefit"]):
                    #    self._update_new_sale(game["node"]["id"], game["node"]["display_name"], game["node"]["cover_landscape_image"]["uri"], game["node"]["current_offer"]["end_time"], game["node"]["current_offer"]["promo_benefit"])
                    #    logging.info(f'Different sale: {game["node"]["display_name"]}')
                    
            # Now, we have all the sales updated in Database
            # First we created a message with ended offers
            new_sales = self._get_current_sales()
            enddoffers = ""
            enditems = self._get_ended_offers()
            for item in enditems:
                enddoffers += "-" + item[0] + "\n"
            # Send messages to Discord
            #offers = self._get_current_no_already_published_sales()
            offers = self._get_current_sales()
            permanentoffers = ""
            saleoftheday = ""
            saleofthedayurl = ""
            current_offers = ""
            currentdate = ""
            sale = True
            channel = discord.utils.get(self.bot.guilds[0].channels, name=self.config["channel"])
            # display_name 0 , expires 1 , imageURL 2, promo 3, currentprice 4, price 5
            #for offer in offers:
            #    if offer[1] == 0:
            #        if offer[3] != None:
            #            permanentoffers+= "-" + offer[0] + " **[" + str(offer[3]) +"]**\n" 
            #    elif sale:
            #        saleoftheday += "\nAvailable until \n📆 **" + datetime.utcfromtimestamp(offer[1]).strftime('%Y-%m-%d %H:%M:%S') + " UTC **\n"
            #        saleoftheday += "\n🤑 " + offer[0] + " **[" + str(offer[3]) +"]**\n** **\n" 
            #        saleofthedayurl = offer[2]
            #        sale = False
            #    else:
            #        if offer[1] != currentdate:
            #            currentdate = offer[1]
            #            current_offers+="📆 **" + datetime.utcfromtimestamp(offer[1]).strftime('%Y-%m-%d %H:%M:%S') +" UTC **\n** **\n"
            #            current_offers += "-" + offer[0] + " **[" + str(offer[3]) +"]**\n" 
            #        else:
            #            current_offers += "-" + offer[0] + " **[" + str(offer[3]) +"]**\n"

            if (enddoffers != ""):
                embedmessage = discord.Embed()
                embedmessage.title = "Go Sales Available"
                embedmessage.set_author(name="😭 Ended Sales 😭")
                embedmessage.url = "https://www.oculus.com/experiences/go/section/1500175860035862/"
                embedmessage.colour = discord.Colour.gold()
                embedmessage.add_field(name="__Ended Sales__", value=enddoffers[:1000],inline=False)
                if role == None:
                    await channel.send(embed=embedmessage)
                else:
                    await channel.send("\n" + role.mention + "\n", embed=embedmessage)
            
            
            for offer in offers: 
                embedmessage = discord.Embed()
                embedmessage.title = offer[0]
                embedmessage.set_author(name="💰 New Sale 💰")
                embedmessage.url = "https://www.oculus.com/experiences/go/" + offer[4]
                embedmessage.colour = discord.Colour.gold()
                embedmessage.set_image(url=offer[2])
                value = "Promotion: [" + offer[3] + "]\n"
                value += "\n**" + offer[5] + "**  -  ~~" + offer[6]   + "~~\n "
                if offer[1] != 0:
                    value += "\nAvailable until\n📆 **" + datetime.utcfromtimestamp(offer[1]).strftime('%Y-%m-%d %H:%M:%S') +" UTC **\n** **\n"
                embedmessage.add_field(name=offer[0], value=value,inline=False)
                if role == None:
                    await channel.send(embed=embedmessage)
                else:
                    await channel.send("\n" + role.mention + "\n", embed=embedmessage)
            
          
            # Set published offers
            #embedmessage = discord.Embed()
            #embedmessage.title = "Go Sales Available"
            #embedmessage.set_author(name="💰 New Sales 💰")
            #embedmessage.url = "https://www.oculus.com/experiences/go/section/1500175860035862/"
            #embedmessage.set_image(url=saleofthedayurl)
            #embedmessage.colour = discord.Colour.gold()
            #publish = False
            #if (permanentoffers != ""):
            #    embedmessage.add_field(name="__Other Offers__", value=permanentoffers,inline=False)
            #    publish = True
            #if (saleoftheday != ""):
            #    embedmessage.add_field(name=" __Featured sale__", value=saleoftheday,inline=False)
            #    publish = True
            #if (current_offers != ""):
            #    embedmessage.add_field(name="__New Offers__", value=current_offers,inline=False)
            #    publish = True
            #if (enddoffers != ""):
            #    embedmessage.add_field(name="__Ended Sales__", value=enddoffers,inline=False)
            #    publish = True
            #if (publish):
            #    if role == None:
            #        await channel.send(embed=embedmessage)
            #    else:
            #        await channel.send("\n" + role.mention + "\n", embed=embedmessage)
            self._set_published()
            # Remove expired_offers
            #self._remove_expired_offers()
        except Exception as e:
             logging.error(f"Error: {e}")
             logging.error(f"Error {e}: {sys.exc_info()[0]}")
             logging.error(f"Error {e}: {sys.exc_info()[2]}")
        
    @check_oculus_sales.before_loop
    async def before_check_oculus_sales(self):
        logging.info("Waiting check_oculus_sales to get ready")
        await self.bot.wait_until_ready() 

# initialize plugin
def setup(bot):
	bot.add_cog(OculusSales(bot))
# Remove extension
def teardown(bot):
    bot.remove_cog('oculus-sales')